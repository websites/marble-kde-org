<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>

<script type="text/javascript">
function store(id, value) {
  document.getElementById(id).value = value
}
function adjustClone() {
  document.getElementById('clone').innerHTML = "git clone " + document.getElementById('branch').value + document.getElementById('repo').value + " ~/marble/sources";
}
function adjustCompilation() {
  document.getElementById('compile').innerHTML = "mkdir -p ~/marble/build\ncd ~/marble/build\ncmake " + document.getElementById('reltype').value + document.getElementById('edition').value + "-DCMAKE_INSTALL_PREFIX=" + document.getElementById('destdir').value + " ~/marble/sources\nmake\n" + document.getElementById('install').value 
  adjustRun();
}
function adjustRun() {
  var qmlpath = "";
  var path = document.getElementById('destdir').value + "/bin/";
  if (document.getElementById('destdir').value == '/usr') {
    path = "";
  }
  if (document.getElementById('binary').value == 'marble-touch') {
    qmlpath = 'QML_IMPORT_PATH=' + document.getElementById('destdir').value + "/lib/marble/plugins ";
  }
  
  document.getElementById('run').innerHTML = qmlpath + path + document.getElementById('binary').value
}
</script>

  <div class="container">
  <h3>Getting the Marble Source Code</h3>
  The Marble source code is available in a <a href="https://invent.kde.org/education/marble" target="_blank">Git repository</a>. Make sure <tt>git</tt> is installed on your system and clone the Marble repository. You can choose between <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default btn-sm active" onClick='store("repo", "https://invent.kde.org/education/marble"); adjustClone()'><input type="radio">anonymous</input></label>
      <label class="btn btn-default btn-sm" onClick='store("repo", "git@invent.kde.org:education/marble"); adjustClone()'><input type="radio">KDE developer</input></label>
    </div> access and choose the initial branch to be <div class="btn-group" data-toggle="buttons">
      <label class="btn btn-default btn-sm active" onClick='store("branch", "-b Applications/17.04 "); adjustClone()'><input type="radio">stable</input></label>
      <label class="btn btn-default btn-sm" onClick='store("branch", ""); adjustClone()'><input type="radio">development</input></label>
    </div>. <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="collapse" data-target="#demo" style="margin-left:50px;">
        Help me choose the right branch...
      </button>
 
  <input type="hidden" id="repo" value="git@invent.kde.org:education/marble" />
  <input type="hidden" id="branch" value="-b Applications/17.04 " />
  <pre style="margin-top:10px;" id="clone">git clone -b Applications/17.04 git://invent.kde.org/education/marble ~/marble/sources</pre>
 
  <p>You can clone the sources to any directory on your system. If you have no preferences, choose <tt>~/marble/sources</tt> as that is the directory we will use below.</p>
  
  <div id="demo" class="collapse out">
  <h3>A note on branches...</h3>
  <p>Development happens directly in the <tt>master</tt> branch. If you want to use the latest features or want to contribute to Marble, choose the <tt>master</tt> branch. The code however is not as stable as the one in the release branches.</p>
  
  <p>
  If stability is your main concern, choose the latest stable release branch, which can easily be recognized by the <tt>Applications/</tt> prefix followed by a release date in year/month format, e.g. Applications/16.08 for the release in August 2016.</p>
  
  <p>Here is a list of commonly used branch names:</p>
  <dl class="dl-horizontal">
  <dt>master</dt><dd>Latest development</dd>
  <dt>Applications/yy.mm</dt><dd>Stable release branches</dd>
  <dt>KDE/4.x</dt><dd>Outdated stable release branches</dd>
  </dl>
  
  <p>Marble follows the common KDE release schedule. New features are developed in and merged to the master branch until the soft (hard) feature freeze. Afterwards a stabilizing period in master follows during which no new features and similar changes are committed to master. At the time of the first release candidate <tt>RC1</tt> a new <tt>Applications/year.month</tt> branch is created from the current master branch. Afterwards the master branch is open for development again. The <tt>Applications/year.month</tt> are stable branches that never receive new features.</p>
  <img src="img/dev/release-branches.png" />
  
  <p>Don't worry too much about branches though! Thanks to git switching between branches is as easy as running e.g. <code>git checkout -b Applications/17.04 origin/Applications/17.04</code> and requires no Internet connection. No need to clone the repository again if you got the inital branch wrong above, just switch to it later.</p>
  
  </div>

  <h3>Compiling the Sources</h3>

  Let's assume you cloned the Marble repository like shown above to the directory <tt>~/marble/sources</tt>. You have cmake and a working compiler toolchain (e.g. GCC and related tools) available and the Qt development packages are installed as well.
  
  You choose the build type to be <div class="btn-group" data-toggle="buttons">
    <label class="btn btn-default btn-sm active" onClick='store("reltype", "-DCMAKE_BUILD_TYPE=Debug "); adjustCompilation()'><input type="radio">Debug</input></label>
    <label class="btn btn-default btn-sm" onClick='store("reltype", "-DCMAKE_BUILD_TYPE=Release "); adjustCompilation()'><input type="radio">Release</input></label>
  </div> and install the <div class="btn-group" data-toggle="buttons">
    <label class="btn btn-default btn-sm active" onClick='store("edition", "-DWITH_KF5=TRUE "); adjustCompilation(); store("binary", "marble"); adjustRun()'><input type="radio">KDE</input></label>
    <label class="btn btn-default btn-sm" onClick='store("edition", "-DWITH_KF5=FALSE "); adjustCompilation(); store("binary", "marble-qt"); adjustRun()'><input type="radio">Qt</input></label>
  </div> version of Marble to <div class="btn-group" data-toggle="buttons">
    <label class="btn btn-default btn-sm active" onClick='store("install", "sudo make install"); store("destdir", "/usr/local"); adjustCompilation()'><input type="radio">/usr/local</input></label>
    <label class="btn btn-default btn-sm" onClick='store("install", "sudo make install"); store("destdir", "/usr"); adjustCompilation()'><input type="radio">/usr</input></label>
    <label class="btn btn-default btn-sm" onClick='store("install", "make install"); store("destdir", "~/marble/export"); adjustCompilation()'><input type="radio">~/marble/export</input></label></div> using the following commands:
  
  <input type="hidden" id="reltype" value="-DCMAKE_BUILD_TYPE=Debug " />
  <input type="hidden" id="edition" value="-DWITH_KF5=TRUE " />
  <input type="hidden" id="destdir" value="/usr/local" />
  <input type="hidden" id="install" value="sudo make install" />
  <pre style="margin-top:10px;" id="compile">mkdir -p ~/marble/build
cd ~/marble/build
cmake -DCMAKE_BUILD_TYPE=Debug -DWITH_KF5=TRUE -DCMAKE_INSTALL_PREFIX=/usr/local ~/marble/sources
make
sudo make install</pre>
  
  <input type="hidden" id="binary" value="marble" />

  <h3>Running Marble</h3>
  When the compilation and installation finished successfully, you can execute
  
  <pre style="margin-top:10px;" id="run">/usr/local/bin/marble</pre>
  
  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
