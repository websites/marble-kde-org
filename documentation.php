<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">
  
  <h3>General Documentation</h3>

  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
      <span class="label label-info pull-right">Linux, Mac, Windows</span>
      <h3>Desktop</h3>
      <p>The user manual introduces you to the main elements of the Marble Desktop application (KDE and Qt version). You can open it from within Marble using the <code>F1</code> shortcut (and from the <code>Help</code> menu). You can also access it online at docs.kde.org.</p>
    <a href="http://docs.kde.org/development/en/kdeedu/marble/">
      <button type="button" class="btn btn-primary">
        <span class="glyphicon glyphicon-book"></span> User Manual
      </button>
    </a>
    </p>
    <p>Does the user manual not answer your question? The following resources may be helpful as well.</p>
    <p>
      <a href="http://userbase.kde.org/Marble">
        <button type="button" class="btn btn-default">
          <span class="glyphicon glyphicon-globe"></span> UserBase
        </button>
      </a>    
      <a href="https://techbase.kde.org/Marble/FAQ">
        <button type="button" class="btn btn-default">
          <span class="glyphicon glyphicon-globe"></span> FAQ
        </button>
      </a>    
    </p>
    <p>You can also get in touch with other users and Marble developers.</p>
    <p>
      <a href="http://forum.kde.org/viewforum.php?f=217">
        <button type="button" class="btn btn-default">
          <span class="glyphicon glyphicon-comment"></span> Forum
        </button>
      </a>    
      <a href="irc://irc.freenode.net/marble">
        <button type="button" class="btn btn-default">
          <span class="glyphicon glyphicon-user"></span> IRC
        </button>
      </a>       
    </p>
    </div>
    </div>
  </div>
  
  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
      <span class="label label-info pull-right">Nokia N900, N950, N9</span>
      <h3>Mobile</h3>
      <p>Learn how to make full use of Marble's offline routing and turn-by-turn navigation capabilities on your smartphone with the tutorials from the UserBase Wiki.</p>
    <p>
    <a href="http://userbase.kde.org/Tutorials#Marble">
      <button type="button" class="btn btn-primary">
        <span class="glyphicon glyphicon-globe"></span> UserBase Tutorials       
      </button>
    </a>   
    </p>
    <p>We have a support thread for both the Maemo (N900) as well as the MeeGo (N9/N950) version at maemo.org Talk. Both threads provide additional information about the mobile versions.</p>
      <a href="http://talk.maemo.org/showthread.php?t=67316">
        <button type="button" class="btn btn-default">
          <span class="glyphicon glyphicon-comment"></span> N900
        </button>
      </a>    
      <a href="http://talk.maemo.org/showthread.php?t=82196">
        <button type="button" class="btn btn-default">
          <span class="glyphicon glyphicon-comment"></span> N9/N950
        </button>
      </a> 
    </p>
    </div>
    </div>
  </div>
  
  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
      <span class="label label-info pull-right">All Platforms</span>
      <h3>Developers</h3>
      <p>Our documentation for developers covers using the Marble library in your Qt or Qt Quick (QML)<!-- or Python--> based projects as well as extending Marble using plugins.</p>
    <p>
    <a href="dev-doc.php">
      <button type="button" class="btn btn-primary">
        Developer Documentation
      </button>
    </a>    
    </p>
    </div>
    </div>
  </div>

  <div class="clearfix visible"></div>
  <h3>Further Documentation</h3>
  
  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
    <p>Create your own maps! Information about Marble's map format (.dgml), how to create historic maps and possible sources for maps.</p>
    
    <a href="https://techbase.kde.org/Marble#Mapping_Coordination">
      <button type="button" class="btn btn-default">
        <span class="glyphicon glyphicon-globe"></span> Map Making
      </button>
    </a>
    
    </div>
    </div>
  </div>
  
  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
    <p>During turn-by-turn navigation you can have a speaker announce turns. Learn how to use your own voice here.</p>
    <a href="http://userbase.kde.org/Marble/CustomSpeakers">
      <button type="button" class="btn btn-default">
        <span class="glyphicon glyphicon-volume-up"></span> Custom Speaker Creation
      </button>
    </a>
    </div>
    </div>
  </div>
  
  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
