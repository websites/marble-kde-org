<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body onload="prettyPrint()">
<link href="css/prettify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/prettify.js"></script>
<?php include "navigation.inc"; ?>
  <div class="container">
    
  <div class="alert alert-info">
  <button type="button" class="close" data-dismiss="alert">×</button>
  This page is about using Marble in your project. Looking for information on contributing to Marble instead?
  <a class="btn btn-sm" href="contributors-welcome.php">Learn more &raquo;</a>
  </div>
  
  <p>The Marble library is written in C++ and provides bindings for Qt Quick (QML).<!-- and Python.--> Below you find some examples that demonstrate a basic integration of a map widget provided by Marble into custom C++ and QML<!--and Python--> applications. You might also be interested to learn about</p>
  <ul>
<!--     <li>a <a href="dev-overview">general overview</a> about Marble's structure and dependencies</li> -->
    <li>ways to <a href="sources.php">compile the source code</a> and setup a development environment</li>
    <li><a href="dev-doc.php">developer documentation</a> like API docs</li>
  </ul>
  
  <ul id="codeTab" class="nav nav-tabs">
    <li class="active"><a href="#cplusplus" data-toggle="tab">C++</a></li>
    <li><a href="#qml" data-toggle="tab">QML</a></li>
<!--    <li><a href="#python" data-toggle="tab">Python</a></li>-->
  </ul>
 
  <div id="codeTabContent" class="tab-content">
    <div class="tab-pane fade in active" id="cplusplus">
    
    <p>Integrating Marble in your C++ project is as simple as creating and using an instance of <code>Marble::MarbleWidget</code>. Here's a basic application:</p>
    
    <pre class="prettyprint">
#include &lt;QApplication&gt;
#include &lt;marble/MarbleWidget.h&gt;

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    // Load Marble using OpenStreetMap in Mercator projection
    Marble::MarbleWidget *mapWidget = new Marble::MarbleWidget;
    mapWidget->setProjection(Marble::Mercator);
    mapWidget->setMapThemeId("earth/openstreetmap/openstreetmap.dgml");

    mapWidget->setWindowTitle("Hello Marble!");
    mapWidget->show();
    return app.exec();
}</pre>
      
    <p>Compile the code with your favorite build system, linking against Qt5 and Marble (no need to link against any KF5 libraries):</p>

    <ul id="buildsystemTab" class="nav nav-tabs">
      <li class="active"><a href="#cmake" data-toggle="tab">CMake</a></li>
      <li><a href="#qmake" data-toggle="tab">qmake</a></li>
    </ul>

    <div class="row">
    <div class="col-md-6">

    <div id="buildSystemTabContent" class="tab-content">
      <div class="tab-pane fade in active" id="cmake">

      <p>An appropriate <code>CMakeLists.txt</code> file to build the sample code above (saved to the file <code>main.cpp</code>) using CMake looks like this:</p>

      <pre class="prettyprint">
cmake_minimum_required(VERSION 2.8.12)

project(hello-marble)

find_package(Qt5Widgets REQUIRED)
find_package(Marble REQUIRED)

add_executable(hello-marble  main.cpp)
target_link_libraries(hello-marble  Marble Qt5::Widgets)</pre>

      <p>With those two files created you can compile and run the sample application by:</p>

      <pre class="prettyprint">
cmake .
make
./hello-marble</pre>
      </div>

      <div class="tab-pane fade" id="qmake">

      <p>An appropriate <code>hello-marble.pro</code> file to build the sample code above (saved to the file <code>main.cpp</code>) using qmake looks like this:</p>
      <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="collapse" data-target="#priinstall" style="margin-left:50px;">
        Help me have qmake find the Marble module...
      </button>

      <pre class="prettyprint">
TEMPLATE = app

CONFIG += qt

QT += Marble

SOURCES += \
  main.cpp</pre>

      <p>With those two files created you can compile and run the sample application by:</p>

      <pre class="prettyprint">
qmake hello-marble.pro # or qmake-qt5
make
./hello-marble</pre>
      </div>

      <div id="priinstall" class="collapse out">
        <p>For qmake to know about the Marble module, you have to do one out of these:</p>
        <ul>
        <li>install Marble into the same prefix as your Qt5 installation (e.g. <code>/usr</code>)</li>
        <li>pass <code>-DMARBLE_PRI_INSTALL_USE_QT_SYS_PATHS=ON</code> to cmake when building Marble</li>
        <li>use the environment variable <code>QMAKEPATH</code> to point to the installation prefix of Marble when calling <code>qmake hello-marble.pro</code></li>
        </ul>
      </div>
    </div>

    <p>Further examples can be found in a set of C++ tutorials in <a href="https://techbase.kde.org/Marble/MarbleCPlusPlus">KDE Techbase</a>. They are also part of the Marble sources in the <a href="https://invent.kde.org/education/marble/-/tree/master/examples/cpp"><code>examples/cpp/</code></a> path.</p>

    </div>
    <div class="col-md-6">
    <img src="img/dev/ex-hello-marble.png" />
    </div>
    </div>
    
    </div>

    <div class="tab-pane fade" id="qml">
  
    <div class="row">
    <div class="col-md-6">
    
    <p>Thanks to our QtQuick integration you can use Marble in QML projects. A basic Marble QML application looks like this:</p>
    
    <pre class="prettyprint">
import QtQuick 2.1
import org.kde.marble 0.20

MarbleWidget {
  projection: "Mercator"
  mapThemeId: "earth/openstreetmap/openstreetmap.dgml"
}</pre>
        
    <p><!--Saved as <code>marble.qml</code>, it can be executed using <code>qmlviewer marble.qml</code>. -->Further examples can be found in the Marble sources in the <a href="https://invent.kde.org/education/marble/-/tree/master/examples/qml"><code>examples/qml/</code></a> path.</p>

    </div>
    <div class="col-md-6">
    <img src="img/dev/ex-hello-marble-qml.png" />
    </div>
    </div>
    
    </div>
<!--
    <div class="tab-pane fade" id="python">
    
    <div class="row">
    <div class="col-md-12">
    <p>A basic Marble Python application looks like this:</p>

    <pre class="prettyprint">
from PyQt4.QtCore import *
from PyKDE4.kdeui import *
from PyKDE4.kdecore import *
from PyKDE4.marble import *
import sys

class MainWin (KMainWindow):
    def __init__ (self, *args):
        KMainWindow.__init__ (self)
        self.resize(640, 480)

        self.marble = Marble.MarbleWidget(self)
        self.marble.setMapThemeId("earth/openstreetmap/openstreetmap.dgml")
        self.setCentralWidget(self.marble)

def main():
    appName     = "hello_marble"
    catalog     = ""
    programName = ki18n ("Hello Marble!")
    version     = "1.0"
    description = ki18n ("Simple Python Marble Example")
    license     = KAboutData.License_GPL
    copyright   = ki18n ("(c) 2008 Your Name")
    text        = ki18n ("none")
    homePage    = "www.example.com"
    bugEmail    = "example@example.com"

    aboutData   = KAboutData (appName, catalog, programName, version, description,
                              license, copyright, text, homePage, bugEmail)

    KCmdLineArgs.init(sys.argv, aboutData)

    app = KApplication()
    mainWindow = MainWin(None, "main window")
    mainWindow.show()
    app.connect(app, SIGNAL ("lastWindowClosed ()"), app.quit)
    app.exec_()

main()
</pre>

    </div>
    </div>

    <div class="row">
    <div class="col-md-6">
    <p>Saved as <code>marble.py</code>, it can be executed using <code>python marble.py</code>. Further examples can be found in the Marble sources in the <a href="https://invent.kde.org/education/marble/-/tree/master/examples/python"><code>examples/python/</code></a> path.</p>

    <p>If you Marble installation does not include the Python bindings, you can install Marble from source. Make sure to add the <code>-DEXPERIMENTAL_PYTHON_BINDINGS=TRUE</code> parameter when calling <code>cmake</code>. Before installing the Python bindings for Marble, <a href="http://www.riverbankcomputing.com/software/pyqt/download">PyQt4</a> and <a href="http://techbase.kde.org/Development/Languages/Python">PyKDE</a> as well as <a href="http://www.riverbankcomputing.com/software/sip/download">SIP</a> must be installed.</p>
    </div>

    <div class="col-md-6">
    <img src="img/dev/ex-hello-marble.png" />
    </div>
    </div>

  </div>
-->
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
