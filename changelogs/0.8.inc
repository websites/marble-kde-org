<div>
<h3><a name="description">Visual Changelog: Marble 0.8</a></h3>

 <p>Marble 0.8 got released together with KDE 4.3 on August 4th 2009. Numerous changes have been made since Marble 0.7:</p>

<h4>Support for Online Services: Wikipedia and FlickR</h4>

<p>
<dl> <dt> <a href="img/screenshots/0.8/onlineservice2.png" target="_blank"><img border="0" width="410" height="317" src="img/screenshots/0.8/onlineservice2_thumb.png" alt="Online Services in Marble" /></a> </dt> <dd><i>Online Services in Marble (Wikipedia and FlickR)</i></dd> </dl>
</p>

<p>In preparation to <i>Google Summer of Code 2009</i> support for <b>Online Service Plugins</b> was integrated with Marble 0.8. <i>Bastian Holst</i> created a <b>Wikipedia Plugin</b> which displays georeferenced Wikipedia articles as an icon on the map. A click onto the icon launches a browser window that displays the referenced site. The data is provided through a webservice via <a href="http://www.geonames.org">GeoNames.org</a>. Likewise a <b>Photo Plugin</b> got created which currently provides photos via the <a href="http://www.flickr.com">FlickR</a> photo sharing site.</p>
<p>The feature lays the foundation for numerous other possible Online Service Plugins. </p>

<p>
<dl> <dt> <a href="img/screenshots/0.8/onlineservice1.png"><img border="0" width="410" height="317" src="img/screenshots/0.8/onlineservice1_thumb.png" alt="Online Services in Marble" target="_blank" /></a> </dt> <dd><i>Online Services in Marble (Wikipedia and FlickR)</i></dd> </dl>
</p>

<h4>Choose among Moons and Planets ...</h4>

<p>
<dl> <dt> <a href="img/screenshots/0.8/planetfilter.png" target="_blank"><img border="0" width="410" height="297" src="img/screenshots/0.8/planetfilter_thumb.png" alt="Planet selection: Venus, Earth, Moon and Mars" /></a> </dt> <dd><i>Planet selection: Venus, Earth, Moon and Mars</i></dd> </dl>
</p>

<p>2009 is the <i>International Year of Astronomy</i>: For this reason we furthermore improved the functionality that is related to the starry sky. With Marble 0.8 it's now possible to select maps by the name of the celestial body: In addition to Earth <b>you can choose among Moon, Venus, Mars and other planets</b>. This feature was implemented by <i>Harshit Jain</i>. Also we've added a list of <b>astronomical observatories</b> (Thanks go to <i>Médéric Boquien</i> for this feature).</p>


<h4>Improved Status Bar: Download Indicator and Tile Level</h4>

<p>
<dl> <dt> <a href="img/screenshots/0.8/statusbar.png" target="_blank"><img border="0" width="410" height="367" src="img/screenshots/0.8/statusbar_thumb.png" alt="Download Indicator and Tile Level" /></a> </dt> <dd><i>Download indicator and status bar configuration menu</i></dd> </dl>
</p>

<p>OpenStreetMap enthusiasts can now easily view the current <b>OSM tile level</b>: A click onto the status bar using the right mouse button offers a menu which lets you customize the appearance of the status bar. Additionally we've added a progress indicator that displays the status of the map data download. All status bar improvements in this release were contributed by <i>Jens-Michael Hoffmann</i>.</p>


<h4>Dynamic Coordinate Grid and Labels ...</h4>

<p>
<dl> <dt> <a href="img/screenshots/0.8/graticule1.png" target="_blank"><img border="0" width="410" height="297" src="img/screenshots/0.8/graticule1_thumb.png" alt="Dynamic Coordinate Grid and Labels" /></a> </dt> <dd><i>Dynamic Coordinate Grid and Labels</i></dd> </dl>
</p>

<p>In all previous versions of Marble the coordinate grid was always displayed without labels and only worked properly for low zoom levels. With Marble 0.8 the coordinate grid got a major overhaul: It now features aligned labels (together with the prime meridian and the tropical circles). The lines now properly adjust to the chosen zoom level. Even the DMS / Decimal setting is taken into account: See <a href="img/screenshots/0.8/graticule2.png" target="_blank">these</a> <a href="img/screenshots/0.8/graticule3.png" target="_blank">two</a> maps of <i>Helgoland</i> and compare the labels closely.</p>


<h4>Performance improvements and more changes under the hood ...</h4>

<p>
<dl> <dt> <a href="img/screenshots/0.8/graphicssystem.png" target="_blank"><img border="0" width="410" height="323" src="img/screenshots/0.8/graphicssystem_thumb.png" alt="Tuning: Choose the Graphics System that works best for you" /></a> </dt> <dd><i>Tuning: Choose the Graphics System that works best for you</i></dd> </dl>
</p>

<p>If you're using Linux and if Marble runs slowly on your computer then you should try to switch the graphics system to <i>Software Rendering</i>. Often enough <i>this will improve performance</i> significantly. Based on Qt 4.5 we introduced this feature as a setting on the "View" tab (see screenshot).</p>

<h4>More Changes</h4>
<ul>
<li>Our long time contributor <i>Patrick Spendrin</i> did a lot of work under the hood to fix bugs and to prepare further extension of the <b>KML support</b>.</li>
<li><i>Eckhart Wörner</i> refactored and largely improved the <b>GPS support</b>.</li>
<li><i>Andrew Manson</i> improved the <b><a href="img/screenshots/0.8/proxy.png" target="_blank">proxy</a> settings</b> by adding Socks5 support.</li>
<li><i>Jens-Michael Hoffmann</i> and <i>Pino Toscano</i> improved the performance of the map download.</li>
<li><i>Bastian Holst</i> added the settings dialog to the Qt-Only version of Marble.</li>
<li><i>Magnus Valle</i> created some more <b>historic maps</b> which still await to be published via GHNS.</li>
<li><i>Simon Edwards</i> provided <b>Python Bindings</b> for Marble.</li>
</ul>

<p>
<dl> <dt> <a href="img/screenshots/0.8/kml1.png" target="_blank"><img border="0" width="410" height="296" src="img/screenshots/0.8/kml1_thumb.png" alt="KML support in Marble" /></a> </dt> <dd><i>KML support in Marble</i></dd> </dl>
</p>
</div>
