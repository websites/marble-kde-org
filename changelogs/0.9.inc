<div>
<h3>Visual Changelog: Marble 0.9</h3>

 <p>Marble 0.9 got released on February 9th, 2010. It's part of the KDE 4.4 Software Compilation.
This latest release features lots of incremental changes.</p>

<h4>Support for Online Services: Weather Plugin, Plugin dialogs</h4>

<p>
<dl> <dt> <a href="img/screenshots/0.9/marble_weather0.png" target="_blank"><img border="0" width="410" height="293" src="img/screenshots/0.9/marble_weather0_thumb.png" alt="Weather Plugin in Marble" /></a> </dt> <dd><i>Weather Plugin in Marble</i></dd> </dl>
</p>

<p>During <i>Google Summer of Code 2009</i> Bastian Holst improved support for
<b>Online Service Plugins</b>. Most notably he added a weather plugin which displays
information about the weather on the map for several weather stations. So you can now look up
the weather condition, temperature, wind direction and wind speed
for any place on earth.
</p>

<p>
<dl> <dt> <a href="img/screenshots/0.9/marble_weather1.png" target="_blank"><img border="0" width="410" height="293" src="img/screenshots/0.9/marble_weather1_thumb.png" alt="Weather Plugin dialogs in Marble" /></a> </dt> <dd><i>Weather Plugin dialogs in Marble</i></dd> </dl>
</p>

<p><i>Bastian Holst</i> also added plugin dialogs to Marble: So it's now possible to easily
add a custom "About Dialog" and "Configuration Dialog" for each plugin. Bastian added such
dialogs for the <b>Wikipedia Plugin</b> and for the <b>Weather Plugin</b>. He also improved
the Wikipedia plugin to show thumbnails of the photos that
are displayed in the article. And he made the amount of Wikipedia links
shown on the map configurable.</p>

<h4>Support custom tile layout Url schemes ("Google Maps Support")</h4>

<p>So far the Url schemes supported by Marble have been hardcoded. This
allowed support for native Marble maps as well as OpenStreetMap support.
But so far there hasn't been any support for arbitrary tile based maps.
Now you can easily add support for your favourite map in Marble by creating a
dgml/xml text file. Technically this allows for support of Google Maps just
by editing a text file. However we feel that it's not legally allowed to
ship Google Maps support with Marble. That's one of the reasons why
Marble 0.9 doesn't ship with Google Maps support. But of course this feature
will allow for easy integration of other maps in the future.</p>
<p>Bernhard Berschow has worked on this feature.</p>

<p>
<dl> <dt> <a href="img/screenshots/0.9/marble_googlemaps0.png" target="_blank"><img border="0" width="410" height="293" src="img/screenshots/0.9/marble_googlemaps0_thumb.png" alt="Google Maps in Marble" /></a> </dt> <dd><i>Google Maps in Marble: Example for a custom Url scheme</i></dd> </dl>
</p>

<p>
<dl> <dt> <a href="img/screenshots/0.9/marble_googlemaps1.png" target="_blank"><img border="0" width="410" height="293" src="img/screenshots/0.9/marble_googlemaps1_thumb.png" alt="Google Maps in Marble" /></a> </dt> <dd><i>Google Maps in Marble: Example for a custom Url scheme</i></dd> </dl>
</p>

<h4>Multitouch support for Marble</h4>

Marco Martin added multitouch support to Marble:

<p>
<dl> <dt> <a href="http://www.youtube.com/watch?v=BtEiB2l1QdM" target="_blank"><img border="0" width="321" height="193" src="img/screenshots/0.9/marble_multitouch.png" alt="Multitouch in Marble" /></a> </dt> <dd><i>Multitouch in Marble</i></dd> </dl>
</p>

<h4>Developer Feature: Marble GeoGraphicsView</h4>

Since Marble 0.9 the library now features initial support for a Marble <a href="http://techbase.kde.org/Projects/Marble/GeoGraphicsViewOverview">GeoGraphicsView</a>.
This change adds a framework that is modelled after QGraphicsView. However in addition
to a classic QGraphicsView the MarbleGraphicsView adds the following features:

<ul>
<li>Support for different projections</li>
<li><a href="http://techbase.kde.org/Projects/Marble/MarbleGeoPainter">GeoPainter</a> support</li>
<li>An API focused on geodetic coordinates ("Longitude"/"Latitude") instead of
screen positions.</li>
</ul>

This allows for easy addition of objects to the Marble map in applications.
Bastian Holst and Andrew Manson have worked on this framework during Marble 0.9
development.

<h4>Performance improvements and more changes under the hood ...</h4>

<p>
In addition to these major improvements our Marble developers have worked on
several other small features, bug fixes and performance improvements:
<ul>
<li>Reload map, Speed improvements (Jens Michael Hoffmann)</li>
<li>Zoom on Double Click / Improved animation support (Dennis Nienhüser)</li>
</ul>
</p>

</div>
