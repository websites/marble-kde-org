<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">

    <div class="tabbable tabs-right">

      <ul id="codeTab" class="nav nav-tabs">
        <li class="active"><a href="#v16" data-toggle="tab">Marble 1.6 / Aug 2013</a></li>
        <li><a href="#v15" data-toggle="tab">Marble 1.5 / Feb 2013</a></li>
        <li><a href="history.php">Marble 1.4 ... 0.6</a></li>
      </ul>

      <div id="codeTabContent" class="tab-content">

        <!-- Marble 1.6 -->
        <div class="tab-pane fade in active" id="v16">
          <h2>Marble 1.6</h2>
          <p>Marble 1.6 was released on August 14th, 2013 as part of the KDE 4.11 release. Enjoy looking over the new and noteworthy:</p>

          <h3>Stars Constellations</h3>
          <p>Finding patterns in prominent stars within apparent proximity on Earth's night sky is a human tradition dating back as far as the Neolithic Age. Marble now renders popular stars constellations thanks to Timothy Lanzi and Torsten Rahn.</p>
          
          <p>
          <object width="640" height="360"><param name="movie" value="https://www.youtube.com/v/gjXa5lBkNOw?version=3"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/gjXa5lBkNOw?version=3" type="application/x-shockwave-flash" width="640" height="360" allowscriptaccess="always" allowfullscreen="true"></embed></object>
          </p>
          
          <h3>Draggable Panels</h3>
          
          <p>
          You might know Marble's user interface with controls on the left side and a large map / globe on the right side. Over the years Marble got more powerful and new controls found their way into the tab bar on the left, which eventually grew short of space. Panels to the rescue! Show the ones (Settings => Panels) you need and close the others. Drag our new panels to your favorite place and stack them on top of each other as you like.
          </p>
          
          <div class="col-md-4"><img class="img-responsive" src="img/screenshots/1.6/panel-legend.png" alt=""></div>
          <div class="col-md-4"><img class="img-responsive" src="img/screenshots/1.6/panel-mapview.png" alt=""></div>
          <div class="col-md-4"><img class="img-responsive" src="img/screenshots/1.6/panel-routing.png" alt=""></div>
          <div class="col-md-4"><img class="img-responsive" src="img/screenshots/1.6/panel-location.png" alt=""></div>
          <div class="col-md-4"><img class="img-responsive" src="img/screenshots/1.6/panel-search.png" alt=""></div>
          <div class="col-md-4"><img class="img-responsive" src="img/screenshots/1.6/panel-files.png" alt=""></div>
          
          <div class="clearfix visual"></div>
          <h3>Navigation Tool Redesign</h3>
          
          <ul class="thumbnails">
            <div class="col-md-6">
              <p>Did you miss a panel with a map zoom and pan control above? Right, there is none. While redesigning our navigation tool on top of the map towards a modern look we fell in love with it so much that we decided a panel providing the same functionality would be of no use. (The old control is still available for developers who use it in their software. We have no plans to remove it.)</p>
              <p>For sure you can continue using shortcuts as well, here's a quick list for your convenience:</p>
                <table class="table table-bordered">
                 <tr><td><tt>+</tt>/<tt>-</tt></td><td>zoom in/out</td></tr>
                 <tr><td>mouse wheel</td><td>zoom in/out</td></tr>
                 <tr><td><tt>Home</tt></td><td>Jump to the home position</td></tr>
                 <tr><td>arrow keys</td><td>pan right/left, up/down</td></tr>
                </table>
            </div>
            <div class="col-md-3"><img src="img/screenshots/1.6/navigation-tool.png" class="img-rounded" alt=""></div>
          </ul>
          
          <div class="clearfix visual"></div>
          <h3>New WebKit Powered Info Dialog</h3>
          
          <p>Many items shown in the map offer additional information when selected (clicked). We redesigned our information dialog from a standalone window to one that sticks to the associated map position, allowing you to continue interaction with the map while it is opened. As an additional benefit the new dialog is based on WebKit and therefore renders even complex websites. Use it to research information about places in Wikipedia, for example.</p>
          
          <p>
           <img class="img-thumbnail" width="690" height="852" src="img/screenshots/1.6/popup.png" class="img-rounded" alt="" />
          </p>
          
          <h3>Map Theme Installation</h3>
          <p>The ability to install additional map themes directly in the application has always been an advantage of Marble's KDE version. We have redesigned this feature to be more concise and show detailed installation progress at the same time. Furthermore it's now possible to open map themes directly from the installation dialog. For the first time the feature is also available in the Marble Qt version. This means that Windows and Mac users can now benefit from the convenient map theme installation as well. Make sure to check out our ever-growing collection of map themes and explore historic maps, street map variants, other planets and much more.</p>
          
          <p>
          <object width="640" height="360"><param name="movie" value="https://www.youtube.com/v/UOtAselHRqw?version=3&amp;hl=de_DE&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/UOtAselHRqw?version=3&amp;hl=de_DE&amp;rel=0" type="application/x-shockwave-flash" width="640" height="360" allowscriptaccess="always" allowfullscreen="true"></embed></object>
          </p>
          
          <h3>License Information Info Box</h3>
            <div class="col-md-5">
              <p>Marble's About dialog provides plenty of information about the origin of data and licenses. In the bottom right corner of the map we now also show license notices for the current map theme. This way giving proper attribution in screenshots becomes much easier. Like all of Marble's map controls the license info can be moved around freely and even hidden.</p>
            </div>
            <div class="col-md-4"><img src="img/screenshots/1.6/license-info.png" class="img-rounded" alt=""></div>
          </ul>
          
          <!--<h3>Major KML I/O Improvements</h3>-->
        </div>


        <!-- Marble 1.5 -->
        <div class="tab-pane fade in" id="v15">
          <h2>Marble 1.5</h2>
          <p>Marble 1.5.0 has been released on February 6th, 2013 as part of the KDE 4.10 release. Please enjoy looking over the new and noteworthy.</p>
          
          <h3>To Moon and Mars: New Space Orbiter visualization</h3>

          <p>René Küttner, a student from the university TU Dresden, worked on Marble 
          as part of the <a href="http://sophia.estec.esa.int/socis2012/">ESA SoCiS 2012 
          program</a>. The Summer of Code in Space was carried out for the second time by 
          the <a href="http://www.esa.int/ESA">European Space Agency</a> and again Marble 
          was chosen as a mentoring organization.</p>

          <p><a href="img/screenshots/1.5/marblekde410.jpg">
            <img class="img-thumbnail" width="640" height="438" src="img/screenshots/1.5/marblekde410.jpg">
          </a></p>

          <p>René developed a visualization of space orbiters around other planets 
          inside the Marble Virtual Globe. As a result Marble can display the position 
          and orbit tracks of space missions such as <a 
          href="http://www.esa.int/Our_Activities/Space_Science/Mars_Express">Mars 
          Express</a>, <a 
          href="http://www.esa.int/Our_Activities/Space_Science/Venus_Express">Venus 
          Express</a> and <a 
          href="http://www.esa.int/Our_Activities/Space_Science/SMART-1">SMART-1</a>. The 
          visualization also includes the positions of the two Mars moons Phobos and 
          Deimos. He also enhanced Marble's display of Earth satellite tracks.
          </p>
          
          <p>
          <iframe width="640" height="360" src="https://www.youtube.com/embed/K_VA0XtvjYk" frameborder="0" allowfullscreen="true"></iframe>
          </p>

          <p>We'd like to thank <a href="http://www.esa.int/ESA">ESA</a> and the <a 
          href="http://sophia.estec.esa.int/socis2012/?q=sponsors">SoCiS Sponsors</a> for 
          funding this project.</p>

          <p><a href="img/screenshots/1.5/marblesocis2012team.jpg">
           <img class="img-thumbnail" width="640" height="360" src="img/screenshots/1.5/marblesocis2012team.jpg">
          </a></p>

          <p>Photo: René Küttner (center) and his project mentors <a 
          href="http://www.scilogs.eu/en/blog/spacetimedreamer/content/about">Gerhard 
          Holtkamp</a> (right) and Torsten Rahn (left) at a privately arranged meeting at 
          <a href="http://www.esa.int/About_Us/ESOC">ESOC</a> in Darmstadt in late 
          December 2012.</p>

          <h3>OpenStreetMap Vector Handling Extensions</h3>
         
          <p>During the summer, Ander Pijoan worked on his GSoC project to implement OpenStreetMap vector tile rendering in Marble.
          Many of his changes are part of Marble 1.5, including improvements in handling relations and vector rendering, extensions to
          Marble's .dgml format to include tiled vector layers and parsing of vector data in json format. You can read about his 
          progress in his <a href="http://blogs.deusto.es/gsoc-deustotech/">blog</a>.
          </p>
          
          <p>The screencast below demonstrates the final state of Ander's project. The map theme shown is not part of Marble 1.5.</p>
          
          <p>
          <object width="640" height="360"><param name="movie" value="https://www.youtube.com/v/lHeA-3G0jKE?version=3&amp;hl=de_DE&amp;rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/UOtAselHRqw?version=3&amp;hl=de_DE&amp;rel=0" type="application/x-shockwave-flash" width="640" height="480" allowscriptaccess="always" allowfullscreen="true"></embed></object>
          </p>
         
        </div>

      </div>
    </div> <!-- /tabbable -->

    <hr />

    <div class="clearfix visual"></div>
        <h2>Version History</h2>
       
    <table class="table table-hover table-condensed" style="width:40%;">

    <tr>
      <th>Release Date</th>
      <td>12/14</td>
      <td>04/15</td>
      <td>08/15</td>
      <td>12/15</td>
      <td>04/16</td>
      <td>08/16</td>
      <td>12/16</td>
      <td>04/17</td>
      <td>08/17</td>
    </tr>
    <tr>
      <th>Desktop</th>
      <td><span class="label label-default">1.10</span></td>
      <td><span class="label label-default">1.11</span></td>
      <td><span class="label label-default">1.12</span></td>
      <td><span class="label label-default">1.13</span></td>
      <td><span class="label label-default">1.14</span></td>
      <td><span class="label label-default">2.0</span></td>
      <td><span class="label label-default">2.1</span></td>
      <td><span class="label label-success">2.2</span></td>
      <td><span class="label label-info">2.3</span></td>
    </tr>
    <tr>
      <th>Library</th>
      <td><span class="label label-default">0.20</span></td>
      <td><span class="label label-default">0.21</span></td>
      <td><span class="label label-default">0.22</span></td>
      <td><span class="label label-default">0.23</span></td>
      <td><span class="label label-default">0.24</span></td>
      <td><span class="label label-default">0.25</span></td>
      <td><span class="label label-default">0.26</span></td>
      <td><span class="label label-success">0.27</span></td>
      <td><span class="label label-info">0.27</span></td>
    </tr>
    <tr>
      <th>KDE Applications</th>
      <td><span class="label label-default">14.12</span></td>
      <td><span class="label label-default">15.04</span></td>
      <td><span class="label label-default">15.08</span></td>
      <td><span class="label label-default">15.12</span></td>
      <td><span class="label label-default">16.04</span></td>
      <td><span class="label label-default">16.08</span></td>
      <td><span class="label label-default">16.12</span></td>
      <td><span class="label label-success">17.04</span></td>
      <td><span class="label label-info">17.08</span></td>
    </tr>

    </table>
        
    <table class="table table-hover table-condensed">

    <tr>
      <th>Release Date</th>
      <td>07/08</td>
      <td>01/09</td>
      <td>08/09</td>
      <td>02/10</td>
      <td>08/10</td>
      <td>01/11</td>
      <td>04/11</td>
      <td>08/11</td>
      <td>01/12</td>
      <td>08/12</td>
      <td>02/13</td>
      <td>08/13</td>
      <td>12/13</td>
      <td>04/14</td>
      <td>08/14</td>
    </tr>
    <tr>
      <th>Desktop</th>
      <td><span class="label label-default">0.6</span></td>
      <td><span class="label label-default">0.7</span></td>
      <td><span class="label label-default">0.8</span></td>
      <td><span class="label label-default">0.9</span></td>
      <td><span class="label label-default">0.10</span></td>
      <td><span class="label label-default">1.0</span></td>
      <td><span class="label label-default">1.1</span></td>
      <td><span class="label label-default">1.2</span></td>
      <td><span class="label label-default">1.3</span></td>
      <td><span class="label label-default">1.4</span></td>
      <td><span class="label label-default">1.5</span></td>
      <td><span class="label label-default">1.6</span></td>
      <td><span class="label label-default">1.7</span></td>
      <td><span class="label label-default">1.8</span></td>
      <td><span class="label label-default">1.9</span></td>
    </tr>
    <tr>
      <th>Library</th>
      <td><span class="label label-default">0.6</span></td>
      <td><span class="label label-default">0.7</span></td>
      <td><span class="label label-default">0.8</span></td>
      <td><span class="label label-default">0.9</span></td>
      <td><span class="label label-default">0.10</span></td>
      <td colspan="2"><span class="label label-default">0.11</span></td>
      <td><span class="label label-default">0.12</span></td>
      <td><span class="label label-default">0.13</span></td>
      <td><span class="label label-default">0.14</span></td>
      <td><span class="label label-default">0.15</span></td>
      <td><span class="label label-default">0.16</span></td>
      <td><span class="label label-default">0.17</span></td>
      <td><span class="label label-default">0.18</span></td>
      <td><span class="label label-default">0.19</span></td>
    </tr>
    <tr>
      <th>KDE SC</th>
      <td><span class="label label-default">4.1</span></td>
      <td><span class="label label-default">4.2</span></td>
      <td><span class="label label-default">4.3</span></td>
      <td><span class="label label-default">4.4</span></td>
      <td><span class="label label-default">4.5</span></td>
      <td colspan="2"><span class="label label-default">4.6</span></td>
      <td><span class="label label-default">4.7</span></td>
      <td><span class="label label-default">4.8</span></td>
      <td><span class="label label-default">4.9</span></td>
      <td><span class="label label-default">4.10</span></td>
      <td><span class="label label-default">4.11</span></td>
      <td><span class="label label-default">4.12</span></td>
      <td><span class="label label-default">4.13</span></td>
      <td><span class="label label-default">4.14</span></td>
    </tr>

    </table>

    <span class="label label-default">old stable</span>
    <span class="label label-success">stable</span>
    <span class="label label-info">development</span>    
    
  </div> <!-- /container -->

  <?php include "footer.inc"; ?>

  <script>
  var $ = jQuery.noConflict();
  $(document).ready(function() {
    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
    return $('a[data-toggle="tab"]').on('shown', function(e) {
      return location.hash = $(e.target).attr('href').substr(1);
    });
  });
  </script>

  </body>
</html>
