function prepareDownload(rawkml)
{
    window.URL = window.webkitURL || window.URL;
    window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder;

    var output = document.getElementById('kml-download');
    var prevLink = output.querySelector('a');
    if (prevLink)
    {
      window.URL.revokeObjectURL(prevLink.href);
    }

    var bb = new BlobBuilder();
    bb.append(rawkml);

    const mimetype = 'application/vnd.google-earth.kml+xml';
    const timestamp = (new Date()).toISOString().replace(/:/,'-');
    const feature = document.getElementById('osmfeature').value.replace(/=/, '-');
    output.download = "marble-" + feature + "-" + timestamp + ".kml";
    output.href = window.URL.createObjectURL(bb.getBlob(mimetype));
    output.dataset.downloadurl = [mimetype, output.download, output.href].join(':');
}

function loadXMLDoc(url)
{
  if (window.XMLHttpRequest)
  {
    xhttp=new XMLHttpRequest();
  }
  else
  {
    xhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xhttp.open("GET",url,false);
  xhttp.send("");
  return xhttp.responseXML;
}

function displayResult()
{
  xml=loadXMLDoc(document.getElementById('myurl').value)
  xsl=loadXMLDoc("kml-guide.xsl");
  if (window.ActiveXObject)
  {
    // code for IE
    ex=xml.transformNode(xsl);
    document.getElementById("transformResult").innerHTML=ex;
  }
  else if (document.implementation && document.implementation.createDocument)
  {
    xsltProcessor=new XSLTProcessor();
    xsltProcessor.importStylesheet(xsl);
    resultDocument = xsltProcessor.transformToFragment(xml,document);
    document.getElementById("transformResult").innerHTML = "";
    document.getElementById("transformResult").appendChild(resultDocument);
    var rawkml = document.getElementById("transformResult").innerHTML.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&/g, '&amp;');
    if (document.getElementById("transformResult").innerHTML.length < 20000)
    {
      prettyPrint();
    }
    prepareDownload(rawkml);
  }
}

function selectAll(element)
{
    var doc = document, text = doc.getElementById(element), range, selection;
    if (doc.body.createTextRange)
    {
      range = document.body.createTextRange();
      range.moveToElementText(text);
      range.select();
    }
    else if (window.getSelection)
    {
      selection = window.getSelection();
      range = document.createRange();
      range.selectNodeContents(text);
      selection.removeAllRanges();
      selection.addRange(range);
    }
}

function updateURL(e)
{
  document.getElementById('myurl').value = "http://overpass-api.de/api/xapi?node[bbox=" + map.getBounds().toBBoxString() + "][" + document.getElementById('osmfeature').value + "]";
  if (map.getZoom() < 14 && document.getElementById('size-warning').className.search('in') < 0)
  {
    $('#size-warning').collapse('show')
  }
  else if (map.getZoom() >= 14 && document.getElementById('size-warning').className.search('in') >= 0)
  {
    $('#size-warning').collapse('hide')
  }
}

function store(feature)
{
  document.getElementById('osmfeature').value = feature;
  updateURL();
}
