<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<link href="css/prettify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/prettify.js"></script>
<link rel="stylesheet" href="css/leaflet.css" />
<?php include "navigation.inc"; ?>
  <div class="container">

	<div class="row">
	  <div id="mapid" class="col-md-12" style="height:600px;"></div>
	</div>
	
	<div class="row">
	  <div class="col-md-12">
	    <h3>Sentinel-2 Satellite Map</h3>
	    <p>Found a white spot on the map? We are looking for volunteers to extend the coverage of the map. No programming skills required! See <a href="https://community.kde.org/Marble/Sentinel2MapTheme">KDE Techbase</a>.
	    <p>Join us at our <a href="irc://irc.freenode.net/marble-sentinel"> #marble-sentinel</a> IRC channel on the freenode network (<a href="http://webchat.freenode.net?channels=marble-sentinel&uio=d4">webchat</a>)</li>
	  </div>
	</div>


	<script src="js/leaflet.js"></script>
	<script>

		var mymap = L.map('mapid').setView([37.60, -122.35], 11);

		L.tileLayer('https://maps.kde.org/earth/sentinel2/{z}/{x}/{y}.jpg', {
			maxZoom: 14,
			attribution: 'Source: ESA Sentinel 2 mission. <a href="http://sedas.satapps.org/data-uses-distribution/">Terms and Conditions</a>.',
			id: 'mapbox.streets'
		}).addTo(mymap);
	</script>

  </div> <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
