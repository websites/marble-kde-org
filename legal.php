<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">
  
  <div class="page-header">
  <h1>Legal notices <small>give credit where credit is due</small></h1>
  </div>
  
  <ul>
    <li>This website is based on <a href="http://getbootstrap.com">Bootstrap</a></li>
    <li><a href="http://thenounproject.com/noun/worker/#icon-No2156">Worker</a> designed by <a href="http://thenounproject.com/kriskhoury">Kris Khoury</a> from The Noun Project</li>
    <li><a href="http://thenounproject.com/noun/smartphone/#icon-No4488">Smartphone</a> designed by <a href="http://thenounproject.com/The Art of E">The Art of E</a> from The Noun Project</li>
    <li><a href="css/tomahawk.css">tomahawk.css</a> is based on <a href="http://www.tomahawk-player.org/assets/css/template.css">template.css</a> from tomahawk-player.org</li>
    <li><a href="img/icons/f_logo.png">"f" logo</a> provided by <a href="http://www.facebook.com/brandpermissions/logos.php">facebook.com</a></li>
    <li><a href="http://www.kde.org/stuff/swlabels/part_of_the_kde_family_horizontal_190.png">part of the KDE family</a> is distributed under the LGPL license. KDE, K Desktop Environment and the KDE Logo are trademarks of KDE e.V.</li>
  </ul>

  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
