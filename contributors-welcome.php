<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">

  <div class="alert alert-info">
  <button type="button" class="close" data-dismiss="alert">×</button>
  This page is about contributing to the development of Marble. Looking for information on using Marble in your project instead?
  <a href="dev-intro.php">Learn more &raquo;</a>
  </div>

  <div class="row">
  <div class="col-md-8">
    <p>
    Welcome to Marble! Contributing to this project will make you part of an international team working on a virtual globe and world atlas. There are many different ways you can help out. We are very open to new people!
    </p>

    <p>
    You should be aware of our two main communication channels:
    <ul style="list-style-type:none">
      <li><i class="icon-bullhorn"></i> The <a href="irc://irc.freenode.net/marble">#marble</a> IRC channel on the freenode network (<a href="http://webchat.freenode.net?channels=marble&uio=d4">webchat</a>)</li>
      <li><i class="icon-envelope"></i> The <a href="https://mail.kde.org/mailman/listinfo/marble-devel">marble-devel@kde.org</a> development mailing list (<a href="http://mail.kde.org/pipermail/marble-devel/">archives</a>)</li>
    </ul>
    Browsing the mailing list archives and hanging around in IRC might be a good idea to get a feeling of current developments. Don't be afraid to raise your voice and ask questions.
    When working on non-trivial patches and larger features, please make everyone aware of it via the usual communication channels (see above).
    </p>
  </div>
  <div class="col-md-4">
     <script type="text/javascript" src="https://www.openhub.net/p/marble/widgets/project_factoids_stats?format=js"></script>
  </div>
  </div>

  <h3>Working Areas</h3>
  <p>
    Making maps, writing code, creating artwork &mdash; there are many different things to work on. Here's a high-level overview. More concrete tasks and things currently being worked on are listed in the <a href="dashboard.php">Dashboard</a>.
  </p>

  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
    <div style="min-height:140px;">
      <h3>Code</h3>
      <p>You like to write source code and feel like spending some time improving Marble? Let us know, we're looking forward to help you getting started!</p>
    </div>
    <p><a class="btn btn-primary" href="https://community.kde.org/Marble/GoMarble"><i class="icon-globe icon-white"></i> Community Wiki</a></p>
    </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
    <div style="min-height:140px;">
      <h3>Documentation</h3>
      <p>By providing high quality and up-to-date documentation, you will make a big impact on helping people understand Marble.</p>
    </div>
    <p>
    <a href="http://www.kde.org/documentation/" class="btn btn-primary"><i class="icon-globe icon-white"></i> Manual</a>
    <a href="http://userbase.kde.org/Tutorials#Marble" class="btn btn-primary"><i class="icon-globe icon-white"></i> Tutorials</a>
    </p>
    </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
    <div style="min-height:140px;">
      <h3>Maps</h3>
      <p>Extend existing map themes and contribute new ones: Maps based on OpenStreetMap, educational ones, historic maps, other planets, ...</p>
    </div>
    <p>
    <a href="https://community.kde.org/Marble#Mapping_Coordination" class="btn btn-primary"><i class="icon-globe icon-white"></i> Community Wiki</a>
    <a href="http://wiki.openstreetmap.org" class="btn btn-default"><i class="icon-globe"></i> OpenStreetMap</a>
    </p>
    </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
    <div style="min-height:160px;">
      <h3>Website</h3>
      <p>Help improving and maintaining this website. Please tell us about missing or outdated information &mdash; or send a patch even.</p>
    </div>
    <p><a href="https://invent.kde.org/websites/marble-kde-org" class="btn btn-primary"><i class="icon-globe icon-white"></i> Website sources</a></p>
    </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
    <div style="min-height:160px;">
      <h3>Artwork</h3>
      <p>Marble needs artists! There are many fun things to improve: Oxygen-style icons, map legends, points of interest, the look-and-feel of the route. Or what about a cartoon style map for children?</p>
    </div>
    <p><a href="https://community.kde.org/Marble#User_Interface" class="btn btn-primary"><i class="icon-globe icon-white"></i> Community Wiki</a></p>
    </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
    <div style="min-height:160px;">
      <h3>Voice Navigation</h3>
      <p>Become the voice of Marble! Guide people on their journey by contributing localized turn-by-turn navigation phrases.</p>
    </div>
    <p><a href="https://community.kde.org/Marble/VoiceOfMarble" class="btn btn-primary"><i class="icon-globe icon-white"></i> Community Wiki</a></p>
    </div>
    </div>
  </div>

  <video width="854" height="480" controls="controls" style="padding:20px;">
    <source src="https://files.kde.org/marble/video/code-swarm/marble-code-swarm.webm" type="video/webm" />
    <source src="https://files.kde.org/marble/video/code-swarm/marble-code-swarm.mp4" type="video/mp4" />
  Your browser does not support the video tag. You can download it in <a href="https://files.kde.org/marble/video/code-swarm/marble-code-swarm.webm">webm</a> or <a href="https://files.kde.org/marble/video/code-swarm/marble-code-swarm.mp4">mp4</a> format however and view it with a media player like <a href="http://www.videolan.org/vlc/">VLC</a>.
  </video>

  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
