<footer style="margin:0; margin-top:40px;">
  <div class='container'>
    <div class='row'>
      <div class='col-md-3'>
	<nav>
	  <ul>
	    <li>About: <a href="about.php">Us</a></li>
	    <li><a href="https://mail.kde.org/mailman/listinfo/marble-devel">Contact</a></li>
	    <li><a href="legal.php">Legal</a></li>
	  </ul>
	</nav>
      </div>
      <div class='col-md-4'>
	<nav>
	  <ul>
	    <li>Resources: <a href="https://invent.kde.org/education/marble">Git</a></li>
	    <li><a href="https://community.kde.org/Marble">Wiki</a></li>
	    <li><a href="https://phabricator.kde.org/differential/query/mge_OXJRp1Hp/">Code Review</a></li>
	  </ul>
	</nav>
      </div>
      <div class='col-md-4'>
	<nav>
	  <ul>
	    <li>Support: <a href="http://forum.kde.org/viewforum.php?f=217">Forums</a></li>
	    <li><a href="irc://irc.freenode.net/marble">IRC</a></li>
            <li><a href="https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=CONFIRMED&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;product=marble">Bug Reports</a></li>
	  </ul>
	</nav>
      </div>
    </div>

    <br />
    <div class="row">
      <div class="col-md-3">
	<div id="credits">
	  <p><img src="img/icons/part_of_the_kde_family_horizontal_190.png" alt="KDE" /></p>
	</div>
      </div>
      <div class="col-md-4">
	<div>
	  <p><a href="http://www.osgeo.org/" target="_blank"><img src="img/icons/OSGeo_project.png" /></a></p>
	</div>
      </div>
      <div class='col-md-2'>
	<a href="http://plus.google.com/109002795119670287274?prsrc=3" rel="publisher" style="text-decoration:none;">
	  <img src="http://ssl.gstatic.com/images/icons/gplus-32.png" alt="Google+" style="border:0;width:32px;height:32px;margin-top:10px;margin-right:5px;"/>
	</a>
	<a href="http://www.facebook.com/marbleglobe"><img src="img/icons/f_logo.png" alt="Facebook" width="32" height="32" style="margin-top:10px;margin-right:10px;" /></a>
      </div>
      <div class="col-md-2">
	<!-- Place this tag where you want the +1 button to render -->
	<div class="g-plusone" data-size="medium" data-annotation="inline" data-width="200" data-href="http://marble.kde.org"></div>
      </div>

      <!-- Place this render call where appropriate -->
      <script type="text/javascript">
	(function() {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	po.src = 'https://apis.google.com/js/plusone.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	})();
      </script>
    </div>
  </div>
</footer>

<!-- Le javascript
  ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>$('.dropdown-toggle').dropdown();</script>
