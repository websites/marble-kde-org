<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body onload="prettyPrint()">
<link href="css/prettify.css" type="text/css" rel="stylesheet" />
<link href="css/devguide.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/prettify.js"></script>
<?php include "navigation.inc"; ?>
  <div class="container">
  <h1 class="page-header">The Marble Developers Guide</h1>

  <div class="row">
  <div class="col-md-10">

   <table class="table table-condensed table-striped pull-right" style="width: 200pt;">
     <tr><td>Authors</td><td>
                Dennis Nienhüser<br/>
            </td></tr>
                <tr><td>Last Update</td><td>August 16th, 2014</td></tr>
            <tr><td>Variants</td><td>
        <a href="http://files.kde.org/marble/devguide/marble-dev-guide.epub"><button type="button" class="btn btn-success btn-sm btn-lg">
          eBook <span class="glyphicon glyphicon-book"></span>
        </button></a>
        <a href="http://files.kde.org/marble/devguide/marble-dev-guide.pdf"><button type="button" class="btn btn-success btn-sm btn-lg">
          PDF <span class="glyphicon glyphicon-file"></span>
        </button></a>
     </td></tr>
   </table>

    <p>Welcome! This guide assists in setting up Marble in development environments and provides a detailed overview of the Marble library. It is directed mostly at</p>
<ul>
<li>developers who want to use the Marble library in their project</li>
<li>packagers who want to generate binary packages for installing Marble and</li>
<li>contributors who want to help making Marble yet more versatile and polished.</li>
</ul>
<p>End users might find some useful information also, but are often better served just <a href="http://marble.kde.org/install.php">installing</a> Marble and, if needed, reading the <a href="http://marble.kde.org/documentation.php">end-user documentation</a> or requesting <a href="http://marble.kde.org/support.php">support</a>.</p>
<p>The Marble project consists of software (LGPL 2.1 license) and data (some free license). The vast majority of its features are contained in the library portions (<code>libmarblewidget</code>, <code>libastro</code> and <code>plugins</code> sum up to 140k SLOC) and hence available to 3rd party applications. The Marble application end-users typically interact with (Marble KDE or Marble Qt) are each just thin wrappers that expose the library features, each less than 3k SLOC. A high-level overview of the existing modules looks like follows:</p>
<div class="figure">
<img src="img/dev/modules.png" alt="Marble Modules" /><p class="caption">Marble Modules</p>
</div>
<p>The remaining parts of the developers guide are organized into three chapters. <a href="#building">Building Marble</a> describes how to retrieve the Marble sources, build them, set up a development environment, install Marble and create packages/installers. The <a href="#architecture">Architecture and Concepts</a> looks into the code contained within the library portions: What are the most important classes, how do they interact? Finally we consider how to extend Marble by new plugins, map themes and how to embed it into 3rd party applications in the <a href="#extending">Extending Marble</a> chapter.</p>
<h1 id="building"><a href="#building">Building Marble</a></h1>
<p>Retrieving the source code, compiling and installing it and executing Marble can be accomplished with just a few commands:</p>
<pre><code>git clone https://invent.kde.org/education/marble ~/marble/sources
mkdir -p ~/marble/build
cd ~/marble/build
cmake ~/marble/sources
make
sudo make install</code></pre>
<p>For day-to-day development, packaging and other scenarios you need more control though. The following sections look into various build aspects.</p>
<h2 id="sources"><a href="#sources">Obtaining the Source Code</a></h2>
<p>The Marble source code is maintained in a git repository. The clone URL is</p>
<ul>
<li><code>https://invent.kde.org/educationmarble</code> (read-only/anonymous access)</li>
<li><code>git@invent.kde.org:education/marble</code> (write access for KDE developers)</li>
</ul>
<p>There are also web interfaces for quickly browsing the sources at <a href="https://invent.kde.org/education/marble">invent.kde.org</a> and <a href="https://projects.kde.org/projects/kde/kdeedu/marble/repository">projects.kde.org</a>. The latter provides alternative access methods (http and tarballs) also. Note that despite the different URLs all point to the same repository.</p>
<p>Retrieving the Marble sources by anonymous access and storing them at <code>~/marble/sources</code> then becomes</p>
<pre><code>git clone https://invent.kde.org/education/marble ~/marble/sources</code></pre>
<p>Development happens in feature branches or (most often) directly in the <code>master</code> branch. Although the <code>master</code> branch contains the latest and greatest features, using it in production environments is not recommended. Stable (release) branches are those starting with <code>KDE/4.</code>, e.g. <code>KDE/4.14</code>. They are created and maintained according to the <a href="https://techbase.kde.org/Schedules">KDE Release Schedules</a>. The most recent stable branch can be determined using</p>
<pre><code>git branch -r | grep KDE | sort -V</code></pre>
<p>which reports said branch on the last line, e.g. <code>origin/KDE/4.14</code>. It can then be checked out using the command</p>
<pre><code>git checkout -b KDE/4.14 origin/KDE/4.14</code></pre>
<h2 id="source-code-organization"><a href="#source-code-organization">Source Code Organization</a></h2>
<p>Let's have a quick look at the directory tree of the Marble sources, shortened to the most important directories:</p>
<pre><code>src/
  apps/
    marble-ui/                Code shared between Marble KDE and Marble Qt
    marble-qt/                Marble application (Qt variant)
    marble-kde                Marble application (KDE variant)
    marble-mobile/            Marble application (Maemo variant)
    marble-touch/             Marble application (MeeGo variant)
  lib/
    marble/                   The heart of Marble
    astro/                    Astronomical calculations
  plugins/
    designer/                 Qt Designer integration
    declarative/              Qt Declarative support
    positionprovider/         Location providers (e.g. GPS)
    render/                   Float items and other display related
    runner/                   Parsing, searching, routing, reverse geocoding
  bindings/
    python/                   Python bindings (only compile in release branches)
tests/                        Unit tests
tools/                        Tools for e.g. data conversion
examples/                     Programming and data examples
  cpp/                        Using libmarblewidget from C++
  qml/                        Using libmarblewidget from QML
  python/                     Using libmarblewidget from Python
data/
  maps/                       Map themes</code></pre>
<p>Remember the high-level overview of the Marble modules? <code>src/lib/marble</code>, <code>src/lib/astro</code> and <code>src/plugins</code> correspond to the libraries, while <code>src/apps</code>, <code>tests</code>, <code>tools</code> and <code>examples</code> reflect the applications.</p>
<h2 id="cmake"><a href="#cmake">CMake Build System</a></h2>
<p>Marble uses CMake, a cross-platform open source build system. Building Marble is quite straightforward in the most simple case and comes down to</p>
<pre><code>cmake /path/to/marble/sources
make
make install</code></pre>
<p>However there are a lot of options to tweak things as needed. This section attempts to list them.</p>
<h3 id="general-options"><a href="#general-options">General Options</a></h3>
<p>A couple of CMake generic and Marble specific options are commonly used to control the build process. The most important ones are</p>
<table>
<thead>
<tr class="header">
<th align="left">Flag</th>
<th align="left">Default</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><code>CMAKE_BUILD_TYPE</code></td>
<td align="left"></td>
<td align="left"><code>Debug</code>, <code>Release</code> or <code>RelWithDebInfo</code></td>
</tr>
<tr class="even">
<td align="left"><code>CMAKE_INSTALL_PREFIX</code></td>
<td align="left"><code>/usr/local</code></td>
<td align="left"><code>/usr</code>, <code>/home/$user/marble/export</code>, ...</td>
</tr>
<tr class="odd">
<td align="left"><code>TILES_AT_COMPILETIME</code></td>
<td align="left"><code>ON</code></td>
<td align="left">Create the data for the Atlas map at compile time?</td>
</tr>
<tr class="even">
<td align="left"><code>BUILD_MARBLE_APPS</code></td>
<td align="left"><code>ON</code></td>
<td align="left">Build Marble applications?</td>
</tr>
<tr class="odd">
<td align="left"><code>BUILD_MARBLE_TESTS</code></td>
<td align="left"><code>OFF</code></td>
<td align="left">Build unit tests?</td>
</tr>
<tr class="even">
<td align="left"><code>BUILD_MARBLE_TOOLS</code></td>
<td align="left"><code>OFF</code></td>
<td align="left">Build tools for data conversion etc.?</td>
</tr>
<tr class="odd">
<td align="left"><code>BUILD_MARBLE_EXAMPLES</code></td>
<td align="left"><code>OFF</code></td>
<td align="left">Build Marble C++ example applications?</td>
</tr>
<tr class="even">
<td align="left"><code>WITH_DESIGNER_PLUGIN</code></td>
<td align="left"><code>ON</code></td>
<td align="left">Build the Qt Designer plugins?</td>
</tr>
<tr class="odd">
<td align="left"><code>BUILD_HARMATTAN_ZOOMINTERCEPTOR</code></td>
<td align="left"><code>OFF</code></td>
<td align="left">Use volume keys for zooming on Maemo?</td>
</tr>
<tr class="even">
<td align="left"><code>BUILD_INHIBIT_SCREENSAVER_PLUGIN</code></td>
<td align="left"><code>OFF</code></td>
<td align="left">Inhibit screensaver during navigation on Maemo?</td>
</tr>
</tbody>
</table>
<p>You can change the default values of the options using CMake specific tools like <code>cmake-gui</code>. Alternatively just pass them on the command line along the lines of <code>cmake -DBUILD_MARBLE_TESTS=ON /path/to/marble/sources</code>.</p>
<h3 id="controlling-dependencies"><a href="#controlling-dependencies">Controlling Dependencies</a></h3>
<p>The only required dependency is Qt. In order for CMake to find Qt, make sure that <code>qmake</code> can be executed from <code>$PATH</code>. If you have multiple versions of Qt installed, check the output of <code>qmake -v</code> to determine which version will be used. Note that <code>qmake</code> alone does not mean Qt is fully installed on your system. You need an installation of Qt that includes development files (headers and optional modules like webkit).</p>
<p>Other libraries can optionally be installed and will be automatically detected by CMake and used. Most prominently kdelibs is among them. For historic reasons Marble treats kdelibs as a required dependency <em>unless you tell it to do otherwise</em>: If you do not want to use the Marble KDE application, pass the <code>QTONLY=ON</code> parameter to CMake and Marble will happily compile without KDE.</p>
<table>
<thead>
<tr class="header">
<th align="left">Dependency</th>
<th align="left">Type</th>
<th align="left">Description</th>
<th align="left">Deactivation</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Qt</td>
<td align="left">Required</td>
<td align="left">the famous cross-platform application framework</td>
<td align="left">N/A</td>
</tr>
<tr class="even">
<td align="left">KDE development platform</td>
<td align="left">Optional</td>
<td align="left">an integrated set of technologies to build applications quickly and efficiently</td>
<td align="left"><code>QTONLY=ON</code></td>
</tr>
<tr class="odd">
<td align="left">quazip</td>
<td align="left">Optional</td>
<td align="left">reading and displaying .kmz files</td>
<td align="left"><code>WITH_quazip=OFF</code></td>
</tr>
<tr class="even">
<td align="left">libshp</td>
<td align="left">Optional</td>
<td align="left">reading and displaying .shp files</td>
<td align="left"><code>WITH_libshp=OFF</code></td>
</tr>
<tr class="odd">
<td align="left">libgps</td>
<td align="left">Optional</td>
<td align="left">position information via gpsd</td>
<td align="left"><code>WITH_libgps=OFF</code></td>
</tr>
<tr class="even">
<td align="left">libwlocate</td>
<td align="left">Optional</td>
<td align="left">Position information based on neighboring WLAN networks</td>
<td align="left"><code>WITH_libwlocate=OFF</code></td>
</tr>
<tr class="odd">
<td align="left">QtLocation</td>
<td align="left">Optional</td>
<td align="left">position information via QtMobility QtLocation</td>
<td align="left"><code>WITH_QtLocation=OFF</code></td>
</tr>
<tr class="even">
<td align="left">liblocation</td>
<td align="left">Optional</td>
<td align="left">(Maemo) position information via liblocation.</td>
<td align="left"><code>WITH_liblocation=OFF</code></td>
</tr>
</tbody>
</table>
<p>If you encounter problems during the <code>cmake</code> run, check its output carefully. CMake reports which dependencies were found or not, and prints an extensive summary of its configuration.</p>
<p>For packaging Marble a system-wide install prefix makes most sense. During development a local installation is usually better though: It does not need root privileges to run <code>make install</code>.</p>
<ul>
<li>Make sure there is no system-wide installation of Marble e.g. from distribution packages. A system-wide installation can typically be detected by checking whether <code>/usr/lib/libmarblewidget.so</code> or <code>/usr/local/lib/libmarblewidget.so</code> exists, though other installation paths are in use also.</li>
<li>You <em>must</em> run <code>make install</code>; a simple <code>make</code> is not enough to execute Marble later on. The reason for this is the installation of required data files.</li>
<li>For a local installation, set <code>WITH_DESIGNER_PLUGIN=OFF</code></li>
<li>For a local installation, the environment variables <code>LD_LIBRARY_PATH</code> and <code>PATH</code> need to be adjusted to include the <code>lib/</code> and <code>bin/</code> directory of the installation directory, respectively.</li>
</ul>
<h3 id="troubleshooting"><a href="#troubleshooting">Troubleshooting</a></h3>
<ul>
<li>Marble starts, but shows a black map only. <em>You did not run <code>make install</code>, or moved the installation directory afterwards.</em></li>
<li>Marble does not start, the shell reports <code>marble: command not found</code>. <em>Call marble or marble-qt with an absolute path, or adjust the $PATH environment variable to include the <code>bin/</code> directory of your installation.</em></li>
<li>Marble does not start, the shell reports <code>error while loading shared libraries: libmarblewidget.so.19: cannot open shared object file: No such file or directory</code>. <em>Setup the <code>LD_LIBRARY_PATH</code> environment variable to include the <code>lib/</code> directory of your installation.</em></li>
<li>Marble crashes during startup with an assertion talking about a hash containing some name. <em>Different versions of libmarblewidget are loaded at the same time. This can happen when an older installation of Marble is still around and old plugins linking against the old installation are trying to be loaded. Remove the old installation.</em></li>
</ul>
<p>In a similar fashion we can compare the directories in the sources to the files that will be installed after compilation and running <code>make install</code> (described further below):</p>
<table>
<thead>
<tr class="header">
<th align="left">Sources</th>
<th align="left">Installation</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><code>src/apps/marble-kde</code></td>
<td align="left"><code>bin/marble</code></td>
</tr>
<tr class="even">
<td align="left"><code>src/apps/marble-qt</code></td>
<td align="left"><code>bin/marble-qt</code></td>
</tr>
<tr class="odd">
<td align="left"><code>src/apps/marble-mobile</code></td>
<td align="left"><code>bin/marble-mobile</code></td>
</tr>
<tr class="even">
<td align="left"><code>src/apps/marble-touch</code></td>
<td align="left"><code>bin/marble-touch</code></td>
</tr>
<tr class="odd">
<td align="left"><code>tests</code></td>
<td align="left">not installed, run <code>make test</code> in the build folder</td>
</tr>
<tr class="even">
<td align="left"><code>tools</code></td>
<td align="left">not installed, execute directly from the build folder</td>
</tr>
<tr class="odd">
<td align="left"><code>examples/cpp/$name</code></td>
<td align="left"><code>share/apps/marble/examples/$name</code></td>
</tr>
<tr class="even">
<td align="left"><code>src/lib/marble/*.cpp</code></td>
<td align="left"><code>lib/libmarblewidget.so</code></td>
</tr>
<tr class="odd">
<td align="left"><code>src/lib/astro/*.cpp</code></td>
<td align="left"><code>lib/libastro.so/</code></td>
</tr>
<tr class="even">
<td align="left"><code>src/plugins/*/$name/*.cpp</code></td>
<td align="left"><code>lib/kde4/plugins/marble/$name.so</code></td>
</tr>
<tr class="odd">
<td align="left"><code>src/lib/marble/*.h</code></td>
<td align="left"><code>include/marble/</code></td>
</tr>
<tr class="even">
<td align="left"><code>src/lib/astro/*.h</code></td>
<td align="left"><code>include/astro/</code></td>
</tr>
<tr class="odd">
<td align="left"><code>data/</code></td>
<td align="left"><code>share/apps/marble/data</code></td>
</tr>
</tbody>
</table>
<h3 id="classic-makefiles-linux"><a href="#classic-makefiles-linux">Classic Makefiles (Linux)</a></h3>
<h3 id="ninja-makefiles-linux"><a href="#ninja-makefiles-linux">Ninja Makefiles (Linux)</a></h3>
<h3 id="visual-studio-solution-ms-windows"><a href="#visual-studio-solution-ms-windows">Visual Studio Solution (MS Windows)</a></h3>
<h3 id="mingw-ms-windows"><a href="#mingw-ms-windows">MinGW (MS Windows)</a></h3>
<h3 id="bundles-macosx"><a href="#bundles-macosx">Bundles (MacOSX)</a></h3>
<h2 id="development-environment"><a href="#development-environment">Development Environment</a></h2>
<h3 id="qtcreator"><a href="#qtcreator">QtCreator</a></h3>
<h3 id="visual-studio"><a href="#visual-studio">Visual Studio</a></h3>
<h2 id="installation-and-packaging"><a href="#installation-and-packaging">Installation and Packaging</a></h2>
<h3 id="system-wide-installation-linux"><a href="#system-wide-installation-linux">System-wide Installation (Linux)</a></h3>
<h3 id="local-installation-linux"><a href="#local-installation-linux">Local Installation (Linux)</a></h3>
<h3 id="nsis-installer-ms-windows"><a href="#nsis-installer-ms-windows">NSIS Installer (MS Windows)</a></h3>
<h1 id="architecture"><a href="#architecture">Architecture and Concepts</a></h1>
<h2 id="marblewidget"><a href="#marblewidget">MarbleWidget</a></h2>
<p><code>MarbleWidget</code> is a <code>QWidget</code> that shows a map and associated information. For many use cases it is the main entry point into the <code>marblewidget</code> library. Many properties and methods are available to customize the appearance of the widget for your needs. By default <code>MarbleWidget</code> is interactive, allowing users to change which parts of the planet are displayed.</p>
<h3 id="minimalistic-code-example"><a href="#minimalistic-code-example">Minimalistic Code Example</a></h3>
<pre class="prettyprint"><code>#include &lt;QApplication&gt;
#include &lt;marble/MarbleWidget.h&gt;

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    Marble::MarbleWidget *mapWidget = new Marble::MarbleWidget;
    mapWidget-&gt;setMapThemeId(&quot;earth/srtm/srtm.dgml&quot;);
    mapWidget-&gt;show();
    return app.exec();
}</code></pre>
<p>Thanks to using sane default values for all properties a basic <code>MarbleWidget</code> is easy to setup. The only thing that needs to be set is the <code>mapThemeId</code> property as explained in the <a href="#mapthemes">Map Themes</a> section. The code above, once compiled and executed, results in an application that looks like this:</p>
<div class="figure">
<img src="img/dev/devguide-marblewidget-1.png" alt="MarbleWidget Screenshot" /><p class="caption">MarbleWidget Screenshot</p>
</div>
<h3 id="most-common-properties-and-methods"><a href="#most-common-properties-and-methods">Most Common Properties and Methods</a></h3>
<p>While <code>MarbleWidget</code> can easily be created, it also provides a versatile configuration. Among the commonly changed properties and methods are:</p>
<ul>
<li>Choosing a planet to display and how it is rendered with the help of the <code>mapThemeId</code> property. See the <a href="#mapthemes">Planets and Map Themes</a> section for details.</li>
<li>Deciding whether the map is shown as a globe or as some variant of a flat map through the <code>projection</code> property. Most commonly <code>Marble::Spherical</code> is used to render a globe and <code>Marble::Mercator</code> for a &quot;flat&quot; map.</li>
<li>Adjusting the part of the map that is shown by the <code>zoom</code>, <code>longitude</code> and <code>latitude</code> parameters or one of their convenience methods. See the <a href="#viewport">Viewport and Projections</a> section for details.</li>
<li>Changing which elements are shown and how they are displayed by the various variants of <code>show*</code> properties as well as the <code>floatItems</code> and <code>renderPlugins</code> methods.</li>
<li>Fine-tuning how the user can interact with <code>MarbleWidget</code> using the <code>popupMenu</code> and <code>inputHandler</code> methods, or disabling input completely using <code>setInputEnabled(false)</code>.</li>
<li>Adding your own rendering in a custom map layer using the <code>addLayer</code> method. The <a href="#layers">Map Layers</a> section covers this in detail.</li>
<li>Managing data related properties by accessing properties and methods of <code>MarbleModel</code> via the <code>model</code> property. See <a href="#marblemodel">MarbleModel</a> for details.</li>
</ul>
<h3 id="qt-designer-integration"><a href="#qt-designer-integration">Qt Designer Integration</a></h3>
<p>'MarbleWidget' can be used from within Qt Designer as described in <a href="#qtdesigner">this section</a>.</p>
<h2 id="marblemodel"><a href="#marblemodel">MarbleModel</a></h2>
<h2 id="mapthemes"><a href="#mapthemes">Planets and Map Themes</a></h2>
<p>While most importantly used to display maps of planet Earth, Marble can render maps of other planets or moons just as fine. All it takes is a <a href="#dgml">DGML</a> file which assembles the associated image and/or vector data and provides meta data like a name or the radius of the planet. Such a DGML file can be loaded using <code>MarbleWidget::setMapThemeId</code>; the associated map theme is loaded as well as the corresponding planet.</p>
<p>Typically you do not have to deal with planets directly, however. Instead one of the ready-made map themes can easily be loaded for display in <a href="#marblewidget">MarbleWidget</a> using its <code>mapThemeId</code> property. Map theme IDs follow a <code>$planet/$theme/$theme.dgml</code> naming scheme. Check out the <code>data/maps/</code> directory in the Marble sources for a list of default map themes. A typical Marble installation provides the following ones:</p>
<table>
<thead>
<tr class="header">
<th align="left">Preview</th>
<th align="left">Name</th>
<th align="left">ID</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left"><img src="img/dev/themes/openstreetmap-preview.png" alt="OSM" /></td>
<td align="left">OpenStreetMap</td>
<td align="left">earth/openstreetmap/openstreetmap.dgml</td>
<td align="left">The free Wiki world map</td>
</tr>
<tr class="even">
<td align="left"><img src="img/dev/themes/srtm-preview.png" alt="OSM" /></td>
<td align="left">Atlas</td>
<td align="left">earth/srtm/srtm.dgml</td>
<td align="left">A vector map in Atlas style</td>
</tr>
<tr class="odd">
<td align="left"><img src="img/dev/themes/bluemarble-preview.png" alt="OSM" /></td>
<td align="left">Satellite View</td>
<td align="left">earth/bluemarble/bluemarble.dgml</td>
<td align="left">Earth seen from space</td>
</tr>
<tr class="even">
<td align="left"><img src="img/dev/themes/political-preview.png" alt="OSM" /></td>
<td align="left">Political Map</td>
<td align="left">earth/political/political.dgml</td>
<td align="left">Highlights governmental boundaries with colors</td>
</tr>
<tr class="odd">
<td align="left"><img src="img/dev/themes/citylights-preview.png" alt="OSM" /></td>
<td align="left">Earth at Night</td>
<td align="left">earth/citylights/citylights.dgml</td>
<td align="left">Earth seen from space at night</td>
</tr>
<tr class="even">
<td align="left"><img src="img/dev/themes/plain-preview.png" alt="OSM" /></td>
<td align="left">Plain Map</td>
<td align="left">earth/plain/plain.dgml</td>
<td align="left">A minimalistic vector map</td>
</tr>
<tr class="odd">
<td align="left"><img src="img/dev/themes/schagen1689-preview.png" alt="OSM" /></td>
<td align="left">Historical Map 1689</td>
<td align="left">earth/schagen1689/schagen1689.dgml</td>
<td align="left">More than 400 years old</td>
</tr>
<tr class="even">
<td align="left"><img src="img/dev/themes/temp-july-preview.png" alt="OSM" /></td>
<td align="left">Temperature (July)</td>
<td align="left">earth/temp-july/temp-july.dgml</td>
<td align="left">Average temperature in July</td>
</tr>
<tr class="odd">
<td align="left"><img src="img/dev/themes/temp-dec-preview.png" alt="OSM" /></td>
<td align="left">Temperature (December)</td>
<td align="left">earth/temp-dec/temp-dec.dgml</td>
<td align="left">Average temperature in December</td>
</tr>
<tr class="even">
<td align="left"><img src="img/dev/themes/precip-july-preview.png" alt="OSM" /></td>
<td align="left">Precipitation (July)</td>
<td align="left">earth/precip-july/precip-july.dgml</td>
<td align="left">Average precipitation in July</td>
</tr>
<tr class="odd">
<td align="left"><img src="img/dev/themes/precip-dec-preview.png" alt="OSM" /></td>
<td align="left">Precipitation (December)</td>
<td align="left">earth/precip-dec/precip-dec.dgml</td>
<td align="left">Average precipitation in December</td>
</tr>
<tr class="even">
<td align="left"><img src="img/dev/themes/clementine-preview.png" alt="OSM" /></td>
<td align="left">Moon</td>
<td align="left">moon/clementine/clementine.dgml</td>
<td align="left">Earth's moon recorded by the Clementine spacecraft</td>
</tr>
</tbody>
</table>
<p>Further map themes are available at <a href="http://marble.kde.org/maps-v3.php">marble.kde.org</a>. The Marble applications provide built-in support to easily explore and install them. You can add support for it as well in your application using the <code>MapThemeDownloadDialog</code> class. Other popular maps like Google Maps, Google Satellite, Bing Maps and more can easily be integrated into Marble as well from a technical point of view. From a legal point of view however this is likely problematic and we do not want to promote the usage of maps which we think are not free.</p>
<p>Programmatic access to the list of installed map themes is provided through the <code>MapThemeManager</code> class, which provides a <code>QAbstractItemModel</code> for convenience.</p>
<p>Marble does know about a dozen different planets by itself. They can be accessed using <code>PlanetFactory</code>. For example, <code>PlanetFactory::construct(&quot;earth&quot;)</code> creates an instance of planet Earth. The <code>Planet</code> class is a lightweight class that contains a handful properties mostly needed for calculations. For planets yet unknown to Marble you can instantiate a <code>Planet</code> directly and set the respective properties. Alternatively new planets can also be instantiated from DGML files. This enables dynamic extension of the list of planets without the need to recompile Marble.</p>
<h2 id="layers"><a href="#layers">Rendering in Map Layers</a></h2>
<h2 id="viewport"><a href="#viewport">Viewport and Projections</a></h2>
<p>The <code>zoom</code> property has no unit, but its related properties <code>distance</code> and <code>radius</code> have: <code>distance</code> is the distance in <code>km</code> between the map surface and the virtual camera (increasing the distance zooms out), <code>radius</code> is the radius of the full globe in pixel (increasing the radius zooms in). These three properties are related and changing any of them changes the other two automatically. The <code>longitude</code> and <code>latitude</code> properties are accompanied by several convenience <code>centerOn</code> methods which take points, places or rectangular regions as arguments.</p>
<h2 id="the-tree-model"><a href="#the-tree-model">The Tree Model</a></h2>
<h2 id="geographic-scene"><a href="#geographic-scene">Geographic Scene</a></h2>
<h2 id="runners"><a href="#runners">Runners</a></h2>
<h2 id="search"><a href="#search">Search</a></h2>
<h2 id="routing"><a href="#routing">Routing</a></h2>
<h2 id="location"><a href="#location">Location</a></h2>
<h2 id="plugins"><a href="#plugins">Plugins</a></h2>
<h2 id="float-items"><a href="#float-items">Float Items</a></h2>
<h2 id="dgml"><a href="#dgml">DGML</a></h2>
<h2 id="kml"><a href="#kml">KML</a></h2>
<h2 id="geodata"><a href="#geodata">GeoData</a></h2>
<h2 id="python-bindings"><a href="#python-bindings">Python Bindings</a></h2>
<h1 id="extending"><a href="#extending">Extending Marble</a></h1>
<h2 id="writing-map-themes"><a href="#writing-map-themes">Writing Map Themes</a></h2>
<h2 id="writing-plugins"><a href="#writing-plugins">Writing Plugins</a></h2>
<h2 id="integrating-marble-into-applications"><a href="#integrating-marble-into-applications">Integrating Marble into Applications</a></h2>
<h2 id="qtdesigner"><a href="#qtdesigner">Using Qt Designer Plugins</a></h2>
<p>When Marble is build with the <code>WITH_DESIGNER_PLUGIN=TRUE</code> flag (activated by default), <code>MarbleWidget</code> is available from within Qt Designer and can be embedded into <code>.ui</code> files just like any other <code>QWidget</code>. If <code>MarbleWidget</code> does not show up in Qt Designer, check that:</p>
<ul>
<li>The boolean cmake option WITH_DESIGNER_PLUGIN is activated</li>
<li>The designer plugin is installed in a directory that is among Qt Designer's plugin paths</li>
<li>The running Qt Designer has access to the <code>marblewidget</code> library through <code>LD_LIBRARY_PATH</code> or a comparable library search path setup.</li>
</ul>
<p class="bg-warning">
Please note that <code>MarbleWidget</code> will not show up in Qt Creator's Design Pane. You have to use Qt Designer instead.
</p>

<h2 id="contributing-back"><a href="#contributing-back">Contributing Back</a></h2>
<h3 id="generating-patches"><a href="#generating-patches">Generating Patches</a></h3>
<h3 id="reviewboard"><a href="#reviewboard">Reviewboard</a></h3>
  
  </div>

        <div id="TOC" class="col-md-2" role="main">
      <div class="bs-docs-sidebar hidden-print hidden-xs hidden-sm" role="complementary">
        <ul class="nav affix">
          <li class="nav-header">Table of Contents</li>
        <ul>
        <li><a href="#building">Building Marble</a><ul>
        <li><a href="#sources">Obtaining the Source Code</a></li>
        <li><a href="#source-code-organization">Source Code Organization</a></li>
        <li><a href="#cmake">CMake Build System</a></li>
        <li><a href="#development-environment">Development Environment</a></li>
        <li><a href="#installation-and-packaging">Installation and Packaging</a></li>
        </ul></li>
        <li><a href="#architecture">Architecture and Concepts</a><ul>
        <li><a href="#marblewidget">MarbleWidget</a></li>
        <li><a href="#marblemodel">MarbleModel</a></li>
        <li><a href="#mapthemes">Planets and Map Themes</a></li>
        <li><a href="#layers">Rendering in Map Layers</a></li>
        <li><a href="#viewport">Viewport and Projections</a></li>
        <li><a href="#the-tree-model">The Tree Model</a></li>
        <li><a href="#geographic-scene">Geographic Scene</a></li>
        <li><a href="#runners">Runners</a></li>
        <li><a href="#search">Search</a></li>
        <li><a href="#routing">Routing</a></li>
        <li><a href="#location">Location</a></li>
        <li><a href="#plugins">Plugins</a></li>
        <li><a href="#float-items">Float Items</a></li>
        <li><a href="#dgml">DGML</a></li>
        <li><a href="#kml">KML</a></li>
        <li><a href="#geodata">GeoData</a></li>
        <li><a href="#python-bindings">Python Bindings</a></li>
        </ul></li>
        <li><a href="#extending">Extending Marble</a><ul>
        <li><a href="#writing-map-themes">Writing Map Themes</a></li>
        <li><a href="#writing-plugins">Writing Plugins</a></li>
        <li><a href="#integrating-marble-into-applications">Integrating Marble into Applications</a></li>
        <li><a href="#qtdesigner">Using Qt Designer Plugins</a></li>
        <li><a href="#contributing-back">Contributing Back</a></li>
        </ul></li>
        </ul>
        </ul>
      </div>
    </div>
    
  </div>
  </div>

  <?php include "footer.inc"; ?>

  </body>
</html>
