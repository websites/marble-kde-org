# Building Marble {#building}

Retrieving the source code, compiling and installing it and executing Marble can be accomplished with just a few commands:

~~~
git clone git@invent.kde.org:education/marble ~/marble/sources
mkdir -p ~/marble/build
cd ~/marble/build
cmake ~/marble/sources
make
sudo make install
~~~

For day-to-day development, packaging and other scenarios you need more control though.
The following sections look into various build aspects.

## Obtaining the Source Code {#sources}

The Marble source code is maintained in a git repository. The clone URL is

* `https://invent.kde.org/education/marble` (read-only/anonymous access)
* `git@invent.kde.org:education/marble` (write access for KDE developers)

There are also web interfaces for quickly browsing the sources at [invent.kde.org](http://invent.kde.org/education/marble)
and [projects.kde.org](https://projects.kde.org/projects/kde/kdeedu/marble/repository).
The latter provides alternative access methods (http and tarballs) also.
Note that despite the different URLs all point to the same repository.

Retrieving the Marble sources by anonymous access and storing them at `~/marble/sources` then becomes

~~~
git clone git@invent.kde.org:education/marble ~/marble/sources
~~~

Development happens in feature branches or (most often) directly in the `master` branch.
Although the `master` branch contains the latest and greatest features, using it in production environments is not recommended.
Stable (release) branches are those starting with `KDE/4.`, e.g. `KDE/4.14`.
They are created and maintained according to the [KDE Release Schedules](https://techbase.kde.org/Schedules).
The most recent stable branch can be determined using

~~~
git branch -r | grep KDE | sort -V
~~~

which reports said branch on the last line, e.g. `origin/KDE/4.14`.
It can then be checked out using the command

~~~
git checkout -b KDE/4.14 origin/KDE/4.14
~~~

## Source Code Organization

Let's have a quick look at the directory tree of the Marble sources,
shortened to the most important directories:

~~~
src/
  apps/
    marble-ui/                Code shared between Marble KDE and Marble Qt
    marble-qt/                Marble application (Qt variant)
    marble-kde                Marble application (KDE variant)
    marble-mobile/            Marble application (Maemo variant)
    marble-touch/             Marble application (MeeGo variant)
  lib/
    marble/                   The heart of Marble
    astro/                    Astronomical calculations
  plugins/
    designer/                 Qt Designer integration
    declarative/              Qt Declarative support
    positionprovider/         Location providers (e.g. GPS)
    render/                   Float items and other display related
    runner/                   Parsing, searching, routing, reverse geocoding
  bindings/
    python/                   Python bindings (only compile in release branches)
tests/                        Unit tests
tools/                        Tools for e.g. data conversion
examples/                     Programming and data examples
  cpp/                        Using libmarblewidget from C++
  qml/                        Using libmarblewidget from QML
  python/                     Using libmarblewidget from Python
data/
  maps/                       Map themes
~~~

Remember the high-level overview of the Marble modules?
`src/lib/marble`, `src/lib/astro` and `src/plugins` correspond to the libraries,
while `src/apps`, `tests`, `tools` and `examples` reflect the applications.

## CMake Build System {#cmake}
Marble uses CMake, a cross-platform open source build system. Building Marble is
quite straightforward in the most simple case and comes down to

~~~
cmake /path/to/marble/sources
make
make install
~~~

However there are a lot of options to tweak things as needed.
This section attempts to list them.

### General Options
A couple of CMake generic and Marble specific options are commonly used to control
the build process. The most important ones are

Flag                                    Default         Description
----------------------------------      ------------    ------------------------------------
`CMAKE_BUILD_TYPE`                                      `Debug`, `Release` or `RelWithDebInfo`
`CMAKE_INSTALL_PREFIX`                  `/usr/local`    `/usr`, `/home/$user/marble/export`, ...
`TILES_AT_COMPILETIME`                  `ON`            Create the data for the Atlas map at compile time?
`BUILD_MARBLE_APPS`                     `ON`            Build Marble applications?
`BUILD_MARBLE_TESTS`                    `OFF`           Build unit tests?
`BUILD_MARBLE_TOOLS`                    `OFF`           Build tools for data conversion etc.?
`BUILD_MARBLE_EXAMPLES`                 `OFF`           Build Marble C++ example applications?
`WITH_DESIGNER_PLUGIN`                  `ON`            Build the Qt Designer plugins?
`BUILD_HARMATTAN_ZOOMINTERCEPTOR`       `OFF`           Use volume keys for zooming on Maemo?
`BUILD_INHIBIT_SCREENSAVER_PLUGIN`      `OFF`           Inhibit screensaver during navigation on Maemo?
----------------------------------      ------------    ------------------------------------

You can change the default values of the options using CMake specific tools like `cmake-gui`.
Alternatively just pass them on the command line along the lines of `cmake -DBUILD_MARBLE_TESTS=ON /path/to/marble/sources`.

### Controlling Dependencies
The only required dependency is Qt. In order for CMake to find Qt, make sure
that `qmake` can be executed from `$PATH`. If you have multiple versions of Qt
installed, check the output of `qmake -v` to determine which version will be used.
Note that `qmake` alone does not mean Qt is fully installed on your system.
You need an installation of Qt that includes development files (headers and optional
modules like webkit).

Other libraries can optionally be installed and will be automatically detected
by CMake and used. Most prominently kdelibs is among them. For historic reasons
Marble treats kdelibs as a required dependency *unless you tell it to do otherwise*:
If you do not want to use the Marble KDE application, pass the `QTONLY=ON` parameter
to CMake and Marble will happily compile without KDE.

Dependency                      Type            Description                                                                             Deactivation
-----------                     --------        -------------------------------------------------------------------------------         ----------------------
Qt                              Required        the famous cross-platform application framework                                         N/A
KDE development platform        Optional        an integrated set of technologies to build applications quickly and efficiently         `QTONLY=ON`
quazip                          Optional        reading and displaying .kmz files                                                       `WITH_quazip=OFF`
libshp                          Optional        reading and displaying .shp files                                                       `WITH_libshp=OFF`
libgps                          Optional        position information via gpsd                                                           `WITH_libgps=OFF`
libwlocate                      Optional        Position information based on neighboring WLAN networks                                 `WITH_libwlocate=OFF`
QtLocation                      Optional        position information via QtMobility QtLocation                                          `WITH_QtLocation=OFF`
liblocation                     Optional        (Maemo) position information via liblocation.                                           `WITH_liblocation=OFF`
-----------                     --------        -------------------------------------------------------------------------------         ----------------------

If you encounter problems during the `cmake` run, check its output carefully.
CMake reports which dependencies were found or not, and prints an extensive summary
of its configuration.

For packaging Marble a system-wide install prefix makes most sense.
During development a local installation is usually better though: It does not
need root privileges to run `make install`.

* Make sure there is no system-wide installation of Marble e.g. from distribution packages.
  A system-wide installation can typically be detected by checking whether `/usr/lib/libmarblewidget.so` or `/usr/local/lib/libmarblewidget.so` exists, though other installation paths are in use also.
* You *must* run `make install`; a simple `make` is not enough to execute Marble later on. The reason for this is the installation of required data files.
* For a local installation, set `WITH_DESIGNER_PLUGIN=OFF`
* For a local installation, the environment variables `LD_LIBRARY_PATH` and `PATH` need to be adjusted to include the `lib/` and `bin/` directory of the installation directory, respectively.

### Troubleshooting

* Marble starts, but shows a black map only. *You did not run `make install`, or moved the installation directory afterwards.*
* Marble does not start, the shell reports `marble: command not found`. *Call marble or marble-qt with an absolute path, or adjust the $PATH environment variable to include the `bin/` directory of your installation.*
* Marble does not start, the shell reports `error while loading shared libraries: libmarblewidget.so.19: cannot open shared object file: No such file or directory`. *Setup the `LD_LIBRARY_PATH` environment variable to include the `lib/` directory of your installation.*
* Marble crashes during startup with an assertion talking about a hash containing some name. *Different versions of libmarblewidget are loaded at the same time. This can happen when an older installation of Marble is still around and old plugins linking against the old installation are trying to be loaded. Remove the old installation.*

In a similar fashion we can compare the directories in the sources to the files
that will be installed after compilation and running `make install`
(described further below):

Sources                      Installation
---------------------------  -----------------------------------------------------
`src/apps/marble-kde`        `bin/marble`
`src/apps/marble-qt`         `bin/marble-qt`
`src/apps/marble-mobile`     `bin/marble-mobile`
`src/apps/marble-touch`      `bin/marble-touch`
`tests`                      not installed, run `make test` in the build folder
`tools`                      not installed, execute directly from the build folder
`examples/cpp/$name`         `share/apps/marble/examples/$name`
`src/lib/marble/*.cpp`       `lib/libmarblewidget.so`
`src/lib/astro/*.cpp`        `lib/libastro.so/`
`src/plugins/*/$name/*.cpp`  `lib/kde4/plugins/marble/$name.so`
`src/lib/marble/*.h`         `include/marble/`
`src/lib/astro/*.h`          `include/astro/`
`data/`                      `share/apps/marble/data`
---------------------------  -----------------------------------------------------

### Classic Makefiles (Linux)
### Ninja Makefiles (Linux)
### Visual Studio Solution (MS Windows)
### MinGW (MS Windows)
### Bundles (MacOSX)

## Development Environment
### QtCreator
### Visual Studio

## Installation and Packaging
### System-wide Installation (Linux)
### Local Installation (Linux)
### NSIS Installer (MS Windows)
