<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">
 
  <div class="col-md-3">
    <div class="thumbnail">
    <div class="caption">
    <div style="min-height:140px;">
      <h3>API</h3>
      <p>The documentation for the C++ interface of Marble is available at api.kde.org.</p>
      <p><a class="btn btn-primary" href="https://api.kde.org/marble/html/index.html"><i class="icon-globe icon-white"></i> API Documentation</a></p>
      <p>Further information can be found in TechBase Wiki pages.</p>
      <p><a class="btn btn-default" href="https://techbase.kde.org/Marble"><i class="icon-globe"></i> TechBase Wiki</a></p>
    </div>
    </div>
    </div>
  </div>
  <div class="col-md-5">
    <div class="thumbnail">
    <div class="caption">
    <div style="min-height:140px;">
      <h3>Tutorials and Examples</h3>
      <p>The <tt>examples</tt> directory in Marble's sources has C++ and Qt Quick (QML)<!-- and Python--> examples that give a good idea how to integrate Marble in your project.</p>
    <p>
    <a href="https://invent.kde.org/education/marble/-/tree/master/examples" class="btn btn-primary"><i class="icon-globe icon-white"></i> All examples and tutorials</a></p>
    <p>A couple of these examples have been converted to TechBase Wiki pages for a more convenient Internet access.</p>
    <p>
    <a class="btn btn-default" href="https://techbase.kde.org/Marble/MarbleCPlusPlus"><i class="icon-file"></i> C++</a>
    <!--<a class="btn btn-default" href="https://techbase.kde.org/Marble/MarblePython"><i class="icon-file"></i> Python</a>-->
    <a class="btn btn-default" href="https://techbase.kde.org/Marble/MarbleDBus"><i class="icon-file"></i> DBus</a>
    </p>
    <p>The KML guide informs about using the Keyhole Markup Language in Marble.</p>
    <p><a class="btn btn-default" href="kml-guide.php">KML Guide</a></p>
    </div>
    </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
    <div class="caption">
    <div style="min-height:140px;">
      <h3>Architecture</h3>
      <p>The blog series <i>Marble's Secrets</i> gives an insight how some of Marble's features are implemented.</p>
      <p class="text-warning">Keep in mind that the <i>Marble's Secrets</i> series was written in 2008 and hence now contains some outdated information.</p>
    <p>
      <a class="btn btn-default" href="https://blogs.kde.org/2008/02/09/marbles-secrets-part-i-behind-scenes-marble"><i class="icon-globe"></i> Part I</a>
      <a class="btn btn-default" href="https://blogs.kde.org/2008/02/10/marbles-secrets-part-ii-walking-shoes-slartibartfast"><i class="icon-globe"></i> Part II</a>
      <a class="btn btn-default" href="https://blogs.kde.org/2008/02/12/marbles-secrets-part-iii-earth-download"><i class="icon-globe"></i> Part III</a>
    </p>
    <p>
      Information about several topics is also stored in Techbase Wiki pages.</p>
      <p><a class="btn btn-default" href="https://techbase.kde.org/Marble"><i class="icon-globe"></i> TechBase Wiki</a>
    </p>
    </div>
    </div>
    </div>
  </div>

  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
