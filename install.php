<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">

  <img align="left" alt="worker icon" src="img/icons/desktop.png" style="margin-right:10px; margin-top:15px;" />
  <h2>Linux <small>Desktop</small></h2>

  <div class="col-md-4">
    <a href="img/gallery/marble-desktop-atlas-distance-route.png" class="thumbnail">
      <img src="img/gallery/marble-desktop-atlas-distance-route_thumb.png" alt="" />
    </a>
  </div>
  <div class="col-md-8">
  <div class="row">
  Marble packages are released together with many other <a href="https://www.kde.org/applications/education/">KDE Applications</a> and packaged in all major Linux distributions. Please use the package manager of your distribution for installation.
  <div class="clearfix visual"></div>
  <div class="col-md-4">
  Debian: <tt><a href="http://packages.debian.org/en/stable/marble">marble</a></tt>
  </div>
  <div class="col-md-4">
  Ubuntu: <tt><a href="http://packages.ubuntu.com/search?keywords=marble&amp;searchon=names&amp;suite=all&amp;section=all">marble</a></tt>
  </div>
  <div class="col-md-4">
  openSUSE: <tt><a href="http://software.opensuse.org/package/marble">marble</a></tt>
  </div>
  <div class="col-md-4">
  Linux Mint: <tt><a href="http://community.linuxmint.com/software/view/marble">marble</a></tt>
  </div>
  <div class="col-md-4">
  Fedora: <tt><a href="https://admin.fedoraproject.org/pkgdb/package/marble/">marble</a></tt>
  </div>
  <div class="col-md-4">
  Arch Linux: <tt><a href="https://www.archlinux.org/packages/extra/x86_64/marble/">marble</a></tt>
  </div>
  <div class="col-md-4">
  Gentoo: <tt>kde-base/marble</tt>
  </div>
  </div>
  </div>


    <div class="clearfix visual"></div>
  <img align="left" alt="worker icon" src="img/icons/smartphone.png" style="margin-right:10px; margin-top:15px;" />
  <h2>Marble Maps <small>Android</small></h2>
  
  <div class="col-md-4">
    <a href="img/screenshots/android/Marble-Android-1.0.0.jpg" class="thumbnail">
      <img src="img/screenshots/android/Marble-Android-1.0.0_small.png" alt="" />
    </a>
  </div>
  <div class="col-md-8">
  <p>
    Marble Maps brings the highly detailed OpenStreetMap to your Android devices. It features a crisp, beautiful map with an intuitive user interface. We designed the map style to resemble the OSM standard tile layer, while introducing more vivid colors with a responsive selection of map features. Calculate routes to any destination and have Marble Maps guide you with its turn-by-turn navigation feature.
  </p>
  <p>
  <a href="https://play.google.com/store/apps/details?id=org.kde.marble.maps&amp;utm_source=global_co&amp;utm_medium=prtnr&amp;utm_content=Mar2515&amp;utm_campaign=PartBadge&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img width="200" alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" /></a>
  </p>
  <p>
  If you do not want to use the Google Play Store, you can <a href="https://files.kde.org/marble/downloads/android/">download an APK</a> or <a href="https://community.kde.org/Marble/AndroidCompiling" target="_blank">build the package yourself</a>.
  </p>
  </div>

  
    
  <div class="clearfix visual"></div>
  <img align="left" alt="worker icon" src="img/icons/smartphone.png" style="margin-right:10px; margin-top:15px;" />
  <h2>Behaim Globe <small>Android</small></h2>

  <div class="col-md-4">
    <a href="img/screenshots/android/Behaim-Globe-1.0.0.jpg" class="thumbnail">
      <img src="img/screenshots/android/Behaim-Globe-1.0.0_small.png" alt="" />
    </a>
  </div>
  <div class="col-md-8">
  <p>
    Martin Behaim and collaborators created the globe at the time of Columbus' first sea travel to the west. View fascinating 3D digital imagery from the original globe taken in an elaborate process over several years.
The original globe can be visited in the Germanisches Nationalmuseum (GNM) in Nuremberg, Germany. Thanks to the GNM for making the data available under the CC-BY-SA license.
  </p>
  <p>
  <a href="https://play.google.com/store/apps/details?id=org.kde.marble.behaim&amp;utm_source=global_co&amp;utm_medium=prtnr&amp;utm_content=Mar2515&amp;utm_campaign=PartBadge&amp;pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1"><img width="200" alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" /></a>
  </p>
  </div>
  
  
  
  <div class="clearfix visual"></div>
  <img align="left" alt="worker icon" src="img/icons/desktop.png" style="margin-right:10px; margin-top:15px;" />
  <h2>Windows <small>Desktop</small></h2>

  <div class="col-md-4">
    <a href="img/screenshots/1.9/marble-1.9.0-win32.png" class="thumbnail">
      <img src="img/screenshots/1.9/marble-1.9.0-win32_thumb.png" alt="" />
    </a>
  </div>
  <div class="col-md-8">
  <p>Use this installer for Microsoft Windows. Download an installer for your architecture and execute it. The installer guides you through the installation process.</p>
  <p>
    <a href="https://files.kde.org/marble/downloads/windows/Marble-setup_2.2.0_x86.exe" class="btn btn-primary nav-btn">Marble 2.2.0 (Windows 32-bit)</a>
    <a href="https://files.kde.org/marble/downloads/windows/Marble-setup_2.2.0-1_x64.exe" class="btn btn-primary nav-btn">Marble 2.2.0 (Windows 64-bit)</a>
  </p>
  <p><a href="http://windows.microsoft.com/en-us/windows7/find-out-32-or-64-bit" target="_blank">Is my PC running the 32-bit or 64-bit version of Windows?</a></p>
  <p class="text-warning">Requires Windows Vista or newer. Please build Marble yourself to run on ancient Windows versions like Windows XP.</p>
  </div>

  <div class="clearfix visual"></div>
  <img align="left" alt="worker icon" src="img/icons/desktop.png" style="margin-right:10px; margin-top:15px;" />
  <h2>Mac OS X <small>Desktop</small></h2>

  <div class="col-md-4">
    <a href="img/gallery/marble-desktop-atlas-osx.png" class="thumbnail">
      <img src="img/gallery/marble-desktop-atlas-osx_thumb.png" alt="" />
    </a>
  </div>
  <div class="col-md-8">
  <p>An Apple package to install Marble on Mac OS X.</p>
  <p><a href="http://files.kde.org/marble/downloads/MacOSX/Marble-2.2.0.pkg" class="btn btn-primary nav-btn">Marble 2.2.0 (OS X 64bit)</a></p>
  
  </div>


  

  <!--
  <div class="clearfix visual"></div>
  <img align="left" alt="worker icon" src="img/icons/smartphone.png" style="margin-right:10px; margin-top:15px;" />
  <h2>Maemo <small>Mobile</small></h2>

  <div class="col-md-4">
    <a href="img/gallery/MarbleMaemo-GuidanceModeKarlsruhe_small.jpg" class="thumbnail">
      <img src="img/gallery/MarbleMaemo-GuidanceModeKarlsruhe_small.jpg" alt="" />
    </a>
  </div>
  <div class="col-md-8">
  <p>Marble on your Nokia N900 smartphone.</p>
  <p>
    You can install Marble directly from the <tt>extras</tt> repository (enabled by default) using the package manager of the N900. There's an additional package <tt>Marble Maps</tt> which contains further map themes (e.g. satellite, public transport, other planets) which we recommend to install as well.
  </p>
  <p>
  <a href="http://maemo.org/downloads/product/raw/Maemo5/marble/?get_installfile" class="btn btn-primary nav-btn">Marble 1.5.5 (Maemo)</a>
  <button class="btn btn-default"><a href="http://maemo.org/downloads/product/raw/Maemo5/marble-maps/?get_installfile">Additional Maps</a></button>
  </p>
  <p>
  Further resources:
    <button class="btn btn-default"><a href="http://maemo.org/downloads/product/Maemo5/marble/">App Info</a></button>
    <button class="btn btn-default"><a href="http://userbase.kde.org/Tutorials#Marble">Tutorials</a></button>
    <button class="btn btn-default"><a href="http://talk.maemo.org/showthread.php?t=67316">Support Thread</a></button>
  </p>
  </div>
  -->

  <!-- MeeGo -->
  <!--
  <div class="clearfix visual"></div>
  <img align="left" alt="worker icon" src="img/icons/smartphone.png" style="margin-right:10px; margin-top:15px;" />
  <h2>MeeGo <small>Mobile</small></h2>

  <div class="col-md-4">
    <a href="img/gallery/marble-meego-routing.jpg" class="thumbnail">
      <img src="img/gallery/marble-meego-routing.jpg" alt="" />
    </a>
  </div>
  <div class="col-md-8">
  <p>Marble on your Nokia N9 (or N950) smartphone.</p>
  <p>
    You can install Marble directly from the Ovi store using the application manager of the N9. Alternatively, open the link below with the browser of your N9.
  </p>
  <p>
  <a href="http://store.ovi.mobi/content/249807" class="btn btn-primary nav-btn">Marble 1.3.3 (MeeGo)</a>
  </p>
  <p>
  Further resources:
    <button class="btn btn-default"><a href="http://store.ovi.com/content/249807">App Info</a></button>
    <button class="btn btn-default"><a href="http://talk.maemo.org/showthread.php?t=82196">Support Thread</a></button>
  </p>
  </div>

  <div class="clearfix visual"></div>
  <div class="page-header">
  <h2>Maps and Voice Navigation</h2>
  <ul>
    <li><a href="maps-v3.php">Additional map themes</a></li>
    <li><a href="speakers.php">Speakers for turn-by-turn navigation</a></li>
  </ul>
  -->

  <!-- Other -->
  <!--
  <div class="page-header">
  <h2>Other</h2>

  <ul>
  <li><a href="http://wiki.osgeo.org/wiki/Live_GIS_Disc">OSGeo Live GIS Disc</a>. A live DVD published by OSGeo which ships Marble and other popular Free Geospatial Software.</li>
  <li><a href="http://www.cornelius-schumacher.de/marbleinabox.html">Marble in a Box</a>. A live CD ISO built with SUSE Studio. Some ISOs even come with the full amount of Blue Marble Satellite imagery.<p class="text-warning">Slightly outdated.</p></li>
  <li><p class="text-warning"><a href="http://www.yet-another-geek.org/archives/5-Introducing-my-eeePC-package-repository.html">Asus EeePC</a>. Packages for the Asus EeePC using an old Marble version 0.6.</p></li>
  </ul>
  </div>
  -->

  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
